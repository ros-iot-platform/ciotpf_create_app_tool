
#spec_example.cpfc.yamlをゴニョゴニョした結果の出力がこうなっててほしい！

import rclpy
from rclpy.node import Node
from rclpy.publisher import Publisher
from rclpy.subscription import Subscription

from std_msgs.msg import Bool, String, Float32
from diagnostic_msgs.msg import KeyValue
import functools
from typing import List, Any
from create_app_tool.ciotpf_app_node_base import CiotpfAppNodeBase, StaticSubscriptionDataModel, ManualSubscriptionDataModel, StaticPublisherDataModel, ManualPublisherDataModel, WildCardSubscriptionDataModel

class _AllTopicSubscrberApppNode(CiotpfAppNodeBase):

    APP_NAME = "all_topic_subscriber_app"
    NODE_NAME = "all_topic_subscriber_app_node"

    
    def __init__(self) -> None:

        super().__init__(self.NODE_NAME, self.APP_NAME)


        #Subscriptions
        self.static_subscriptions: List[StaticSubscriptionDataModel] = [
        ]

        self.manual_subscriptions: List[ManualSubscriptionDataModel] = [
        ]

        self.wildcard_subscriptions: List[WildCardSubscriptionDataModel] = [
            WildCardSubscriptionDataModel("AllTopics", "/ciotpf/*", self.all_topics_subscription_callback, Any)
        ]

        #Publishers
        self.static_publishers: List[StaticPublisherDataModel] = [
        ]

        self.manual_publushers: List[ManualPublisherDataModel] = [
        ]

        self.create_static_subscriptions(self.static_subscriptions)
        self.create_wildcard_subscriptions(self.wildcard_subscriptions)
        self.create_static_publishers(self.static_publishers)


    
    def _periodic_task(self) -> None:
        super()._periodic_task()
        

    def all_topics_subscription_callback(self, topicName:str, message: Any):
        raise NotImplementedError()



class AllTopicSubscrberAppNode(_AllTopicSubscrberApppNode):
    #コントロール対象の照明が受け付けている、照明のオンオフ操作トピック
    #lights_publish(self, value: bool) -> None

    #全ての照明のステータス
    #all_status_publish(self, value: str) -> String


    def __init__(self):
        super().__init__()

    #温度計の温度情報
    def all_topics_subscription_callback(self, topicName:str, message: Any):
        #print(message.data)
        print(topicName)
        pass




def main(args=None):
    rclpy.init(args=args)

    node = AllTopicSubscrberAppNode()

    rclpy.spin(node)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()