このツールは何をするか？


AppNodeのSpecificationから雛形を出力

・雛形が持つべき機能
    ①入出力の指定
        - トピック名を指定するかどうか
            指定しないなら、外から指定出来るようにする
                    →外からとは、モバイルアプリなりなんなり
            ワイルドカードを使えるようにする
                →ciotpf/sensor/*
                →ciotpf/actuator/*
                など

    ②人間が読める状態でのそのノードの説明



topic名:
    ~/hogehoge
    にすると、~の部分はAppNodeNameになる。
    manual"にすると、外から設定するようになる
    ワイルドカードは、InputsのみOK.（当然ながらpublishするトピックにワイルドカードは意味がわからん）

全てのこのIoTPFのトピック名は、/ciotpf/から始まるのが前提。(なので/ciotpf/は省く)

Type（データ型）には、一旦以下を指定
bool, float32, int32, string  
あとは、配列とかROS2デフォルトのやつとか独自のやつも設定したい！！！  
https://github.com/ros2/common_interfaces/tree/dashing  



# 初回環境セットアップ時にやること  

ros2_workspaceで、  
colcon build
する・・？しなくてもまあOk  

installディレクトリだけgit commitに含めてしまっている。これで、colcon buildしなくても自分で定義したメッセージが使える。  

docker-baseディレクトリのDockerfileのイメージをビルドする。
このイメージをベースにして色々やってる。



# もろもろ  
utilsみたいなやつは/opt/ciotpfにクローンして、PYTHONPATHに追加して参照できるようにした。  
Dockerfileでやってるから、もし他の環境にこのリポジトリ持ってくなら気をつけて。

# Test

ros2 topic pub /ciotpf/app/light_controll_app/switch_all std_msgs/msg/Bool 'data: True

ros2 service call /ciotpf/light_control_app_app_node2/app_node_info ciotpf_messages/srv/CiotpfAppNodeInfo 


ros2 service call /ciotpf/app/light_control_app/manual_topic_config ciotpf_messages/srv/CiotpfAppNodeManualTopicConfig "{is_subscription: false, is_add: true, topic_name: /ciotpf/app/test, name: Lights}"











## ごみ
ros2 topic pub /ciotpf/config/app/light_controll_app/manual_topics/lights_status diagnostic_msgs/msg/KeyValue "{key: /ciotpf/sensor/myroom/light1/status, value: add}"


ros2 topic pub /ciotpf/config/app/light_controll_app/manual_topics/lights diagnostic_msgs/msg/KeyValue "{key: /test, value: add}"

