// generated from rosidl_generator_cpp/resource/idl__traits.hpp.em
// with input from ciotpf_messages:msg/CiotpfAppNodeInfo.idl
// generated code does not contain a copyright notice

#ifndef CIOTPF_MESSAGES__MSG__CIOTPF_APP_NODE_INFO__TRAITS_HPP_
#define CIOTPF_MESSAGES__MSG__CIOTPF_APP_NODE_INFO__TRAITS_HPP_

#include "ciotpf_messages/msg/ciotpf_app_node_info__struct.hpp"
#include <rosidl_generator_cpp/traits.hpp>
#include <stdint.h>
#include <type_traits>

namespace rosidl_generator_traits
{

template<>
inline const char * data_type<ciotpf_messages::msg::CiotpfAppNodeInfo>()
{
  return "ciotpf_messages::msg::CiotpfAppNodeInfo";
}

template<>
struct has_fixed_size<ciotpf_messages::msg::CiotpfAppNodeInfo>
  : std::integral_constant<bool, false> {};

template<>
struct has_bounded_size<ciotpf_messages::msg::CiotpfAppNodeInfo>
  : std::integral_constant<bool, false> {};

}  // namespace rosidl_generator_traits

#endif  // CIOTPF_MESSAGES__MSG__CIOTPF_APP_NODE_INFO__TRAITS_HPP_
