// generated from rosidl_typesupport_introspection_c/resource/idl__type_support.c.em
// with input from ciotpf_messages:msg/CiotpfAppNodeInfo.idl
// generated code does not contain a copyright notice

#include <stddef.h>
#include "ciotpf_messages/msg/ciotpf_app_node_info__rosidl_typesupport_introspection_c.h"
#include "ciotpf_messages/msg/rosidl_typesupport_introspection_c__visibility_control.h"
#include "rosidl_typesupport_introspection_c/field_types.h"
#include "rosidl_typesupport_introspection_c/identifier.h"
#include "rosidl_typesupport_introspection_c/message_introspection.h"
#include "ciotpf_messages/msg/ciotpf_app_node_info__struct.h"


// Include directives for member types
// Member `subscriptions`
// Member `publishers`
#include "ciotpf_messages/msg/ciotpf_manual_topic.h"
// Member `subscriptions`
// Member `publishers`
#include "ciotpf_messages/msg/ciotpf_manual_topic__rosidl_typesupport_introspection_c.h"

#ifdef __cplusplus
extern "C"
{
#endif

size_t CiotpfAppNodeInfo__rosidl_typesupport_introspection_c__size_function__CiotpfManualTopic__subscriptions(
  const void * untyped_member)
{
  const ciotpf_messages__msg__CiotpfManualTopic__Sequence * member =
    (const ciotpf_messages__msg__CiotpfManualTopic__Sequence *)(untyped_member);
  return member->size;
}

const void * CiotpfAppNodeInfo__rosidl_typesupport_introspection_c__get_const_function__CiotpfManualTopic__subscriptions(
  const void * untyped_member, size_t index)
{
  const ciotpf_messages__msg__CiotpfManualTopic__Sequence * member =
    (const ciotpf_messages__msg__CiotpfManualTopic__Sequence *)(untyped_member);
  return &member->data[index];
}

void * CiotpfAppNodeInfo__rosidl_typesupport_introspection_c__get_function__CiotpfManualTopic__subscriptions(
  void * untyped_member, size_t index)
{
  ciotpf_messages__msg__CiotpfManualTopic__Sequence * member =
    (ciotpf_messages__msg__CiotpfManualTopic__Sequence *)(untyped_member);
  return &member->data[index];
}

bool CiotpfAppNodeInfo__rosidl_typesupport_introspection_c__resize_function__CiotpfManualTopic__subscriptions(
  void * untyped_member, size_t size)
{
  ciotpf_messages__msg__CiotpfManualTopic__Sequence * member =
    (ciotpf_messages__msg__CiotpfManualTopic__Sequence *)(untyped_member);
  ciotpf_messages__msg__CiotpfManualTopic__Sequence__fini(member);
  return ciotpf_messages__msg__CiotpfManualTopic__Sequence__init(member, size);
}

size_t CiotpfAppNodeInfo__rosidl_typesupport_introspection_c__size_function__CiotpfManualTopic__publishers(
  const void * untyped_member)
{
  const ciotpf_messages__msg__CiotpfManualTopic__Sequence * member =
    (const ciotpf_messages__msg__CiotpfManualTopic__Sequence *)(untyped_member);
  return member->size;
}

const void * CiotpfAppNodeInfo__rosidl_typesupport_introspection_c__get_const_function__CiotpfManualTopic__publishers(
  const void * untyped_member, size_t index)
{
  const ciotpf_messages__msg__CiotpfManualTopic__Sequence * member =
    (const ciotpf_messages__msg__CiotpfManualTopic__Sequence *)(untyped_member);
  return &member->data[index];
}

void * CiotpfAppNodeInfo__rosidl_typesupport_introspection_c__get_function__CiotpfManualTopic__publishers(
  void * untyped_member, size_t index)
{
  ciotpf_messages__msg__CiotpfManualTopic__Sequence * member =
    (ciotpf_messages__msg__CiotpfManualTopic__Sequence *)(untyped_member);
  return &member->data[index];
}

bool CiotpfAppNodeInfo__rosidl_typesupport_introspection_c__resize_function__CiotpfManualTopic__publishers(
  void * untyped_member, size_t size)
{
  ciotpf_messages__msg__CiotpfManualTopic__Sequence * member =
    (ciotpf_messages__msg__CiotpfManualTopic__Sequence *)(untyped_member);
  ciotpf_messages__msg__CiotpfManualTopic__Sequence__fini(member);
  return ciotpf_messages__msg__CiotpfManualTopic__Sequence__init(member, size);
}

static rosidl_typesupport_introspection_c__MessageMember CiotpfAppNodeInfo__rosidl_typesupport_introspection_c__CiotpfAppNodeInfo_message_member_array[2] = {
  {
    "subscriptions",  // name
    rosidl_typesupport_introspection_c__ROS_TYPE_MESSAGE,  // type
    0,  // upper bound of string
    NULL,  // members of sub message (initialized later)
    true,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(ciotpf_messages__msg__CiotpfAppNodeInfo, subscriptions),  // bytes offset in struct
    NULL,  // default value
    CiotpfAppNodeInfo__rosidl_typesupport_introspection_c__size_function__CiotpfManualTopic__subscriptions,  // size() function pointer
    CiotpfAppNodeInfo__rosidl_typesupport_introspection_c__get_const_function__CiotpfManualTopic__subscriptions,  // get_const(index) function pointer
    CiotpfAppNodeInfo__rosidl_typesupport_introspection_c__get_function__CiotpfManualTopic__subscriptions,  // get(index) function pointer
    CiotpfAppNodeInfo__rosidl_typesupport_introspection_c__resize_function__CiotpfManualTopic__subscriptions  // resize(index) function pointer
  },
  {
    "publishers",  // name
    rosidl_typesupport_introspection_c__ROS_TYPE_MESSAGE,  // type
    0,  // upper bound of string
    NULL,  // members of sub message (initialized later)
    true,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(ciotpf_messages__msg__CiotpfAppNodeInfo, publishers),  // bytes offset in struct
    NULL,  // default value
    CiotpfAppNodeInfo__rosidl_typesupport_introspection_c__size_function__CiotpfManualTopic__publishers,  // size() function pointer
    CiotpfAppNodeInfo__rosidl_typesupport_introspection_c__get_const_function__CiotpfManualTopic__publishers,  // get_const(index) function pointer
    CiotpfAppNodeInfo__rosidl_typesupport_introspection_c__get_function__CiotpfManualTopic__publishers,  // get(index) function pointer
    CiotpfAppNodeInfo__rosidl_typesupport_introspection_c__resize_function__CiotpfManualTopic__publishers  // resize(index) function pointer
  }
};

static const rosidl_typesupport_introspection_c__MessageMembers CiotpfAppNodeInfo__rosidl_typesupport_introspection_c__CiotpfAppNodeInfo_message_members = {
  "ciotpf_messages__msg",  // message namespace
  "CiotpfAppNodeInfo",  // message name
  2,  // number of fields
  sizeof(ciotpf_messages__msg__CiotpfAppNodeInfo),
  CiotpfAppNodeInfo__rosidl_typesupport_introspection_c__CiotpfAppNodeInfo_message_member_array  // message members
};

// this is not const since it must be initialized on first access
// since C does not allow non-integral compile-time constants
static rosidl_message_type_support_t CiotpfAppNodeInfo__rosidl_typesupport_introspection_c__CiotpfAppNodeInfo_message_type_support_handle = {
  0,
  &CiotpfAppNodeInfo__rosidl_typesupport_introspection_c__CiotpfAppNodeInfo_message_members,
  get_message_typesupport_handle_function,
};

ROSIDL_TYPESUPPORT_INTROSPECTION_C_EXPORT_ciotpf_messages
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, ciotpf_messages, msg, CiotpfAppNodeInfo)() {
  CiotpfAppNodeInfo__rosidl_typesupport_introspection_c__CiotpfAppNodeInfo_message_member_array[0].members_ =
    ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, ciotpf_messages, msg, CiotpfManualTopic)();
  CiotpfAppNodeInfo__rosidl_typesupport_introspection_c__CiotpfAppNodeInfo_message_member_array[1].members_ =
    ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, ciotpf_messages, msg, CiotpfManualTopic)();
  if (!CiotpfAppNodeInfo__rosidl_typesupport_introspection_c__CiotpfAppNodeInfo_message_type_support_handle.typesupport_identifier) {
    CiotpfAppNodeInfo__rosidl_typesupport_introspection_c__CiotpfAppNodeInfo_message_type_support_handle.typesupport_identifier =
      rosidl_typesupport_introspection_c__identifier;
  }
  return &CiotpfAppNodeInfo__rosidl_typesupport_introspection_c__CiotpfAppNodeInfo_message_type_support_handle;
}
#ifdef __cplusplus
}
#endif
