// generated from rosidl_generator_c/resource/idl__struct.h.em
// with input from ciotpf_messages:msg/CiotpfManualTopic.idl
// generated code does not contain a copyright notice

#ifndef CIOTPF_MESSAGES__MSG__CIOTPF_MANUAL_TOPIC__STRUCT_H_
#define CIOTPF_MESSAGES__MSG__CIOTPF_MANUAL_TOPIC__STRUCT_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>


// Constants defined in the message

// Include directives for member types
// Member 'name'
// Member 'topic_type'
#include "rosidl_generator_c/string.h"
// Member 'topic_names'
#include "ciotpf_messages/msg/ciotpf_string_array__struct.h"

// Struct defined in msg/CiotpfManualTopic in the package ciotpf_messages.
typedef struct ciotpf_messages__msg__CiotpfManualTopic
{
  rosidl_generator_c__String name;
  rosidl_generator_c__String topic_type;
  ciotpf_messages__msg__CiotpfStringArray topic_names;
} ciotpf_messages__msg__CiotpfManualTopic;

// Struct for a sequence of ciotpf_messages__msg__CiotpfManualTopic.
typedef struct ciotpf_messages__msg__CiotpfManualTopic__Sequence
{
  ciotpf_messages__msg__CiotpfManualTopic * data;
  /// The number of valid items in data
  size_t size;
  /// The number of allocated items in data
  size_t capacity;
} ciotpf_messages__msg__CiotpfManualTopic__Sequence;

#ifdef __cplusplus
}
#endif

#endif  // CIOTPF_MESSAGES__MSG__CIOTPF_MANUAL_TOPIC__STRUCT_H_
