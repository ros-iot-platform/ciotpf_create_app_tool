// generated from rosidl_typesupport_introspection_c/resource/idl__type_support.c.em
// with input from ciotpf_messages:msg/CiotpfManualTopic.idl
// generated code does not contain a copyright notice

#include <stddef.h>
#include "ciotpf_messages/msg/ciotpf_manual_topic__rosidl_typesupport_introspection_c.h"
#include "ciotpf_messages/msg/rosidl_typesupport_introspection_c__visibility_control.h"
#include "rosidl_typesupport_introspection_c/field_types.h"
#include "rosidl_typesupport_introspection_c/identifier.h"
#include "rosidl_typesupport_introspection_c/message_introspection.h"
#include "ciotpf_messages/msg/ciotpf_manual_topic__struct.h"


// Include directives for member types
// Member `name`
// Member `topic_type`
#include "rosidl_generator_c/string_functions.h"
// Member `topic_names`
#include "ciotpf_messages/msg/ciotpf_string_array.h"
// Member `topic_names`
#include "ciotpf_messages/msg/ciotpf_string_array__rosidl_typesupport_introspection_c.h"

#ifdef __cplusplus
extern "C"
{
#endif

static rosidl_typesupport_introspection_c__MessageMember CiotpfManualTopic__rosidl_typesupport_introspection_c__CiotpfManualTopic_message_member_array[3] = {
  {
    "name",  // name
    rosidl_typesupport_introspection_c__ROS_TYPE_STRING,  // type
    0,  // upper bound of string
    NULL,  // members of sub message
    false,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(ciotpf_messages__msg__CiotpfManualTopic, name),  // bytes offset in struct
    NULL,  // default value
    NULL,  // size() function pointer
    NULL,  // get_const(index) function pointer
    NULL,  // get(index) function pointer
    NULL  // resize(index) function pointer
  },
  {
    "topic_type",  // name
    rosidl_typesupport_introspection_c__ROS_TYPE_STRING,  // type
    0,  // upper bound of string
    NULL,  // members of sub message
    false,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(ciotpf_messages__msg__CiotpfManualTopic, topic_type),  // bytes offset in struct
    NULL,  // default value
    NULL,  // size() function pointer
    NULL,  // get_const(index) function pointer
    NULL,  // get(index) function pointer
    NULL  // resize(index) function pointer
  },
  {
    "topic_names",  // name
    rosidl_typesupport_introspection_c__ROS_TYPE_MESSAGE,  // type
    0,  // upper bound of string
    NULL,  // members of sub message (initialized later)
    false,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(ciotpf_messages__msg__CiotpfManualTopic, topic_names),  // bytes offset in struct
    NULL,  // default value
    NULL,  // size() function pointer
    NULL,  // get_const(index) function pointer
    NULL,  // get(index) function pointer
    NULL  // resize(index) function pointer
  }
};

static const rosidl_typesupport_introspection_c__MessageMembers CiotpfManualTopic__rosidl_typesupport_introspection_c__CiotpfManualTopic_message_members = {
  "ciotpf_messages__msg",  // message namespace
  "CiotpfManualTopic",  // message name
  3,  // number of fields
  sizeof(ciotpf_messages__msg__CiotpfManualTopic),
  CiotpfManualTopic__rosidl_typesupport_introspection_c__CiotpfManualTopic_message_member_array  // message members
};

// this is not const since it must be initialized on first access
// since C does not allow non-integral compile-time constants
static rosidl_message_type_support_t CiotpfManualTopic__rosidl_typesupport_introspection_c__CiotpfManualTopic_message_type_support_handle = {
  0,
  &CiotpfManualTopic__rosidl_typesupport_introspection_c__CiotpfManualTopic_message_members,
  get_message_typesupport_handle_function,
};

ROSIDL_TYPESUPPORT_INTROSPECTION_C_EXPORT_ciotpf_messages
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, ciotpf_messages, msg, CiotpfManualTopic)() {
  CiotpfManualTopic__rosidl_typesupport_introspection_c__CiotpfManualTopic_message_member_array[2].members_ =
    ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, ciotpf_messages, msg, CiotpfStringArray)();
  if (!CiotpfManualTopic__rosidl_typesupport_introspection_c__CiotpfManualTopic_message_type_support_handle.typesupport_identifier) {
    CiotpfManualTopic__rosidl_typesupport_introspection_c__CiotpfManualTopic_message_type_support_handle.typesupport_identifier =
      rosidl_typesupport_introspection_c__identifier;
  }
  return &CiotpfManualTopic__rosidl_typesupport_introspection_c__CiotpfManualTopic_message_type_support_handle;
}
#ifdef __cplusplus
}
#endif
