// generated from rosidl_generator_c/resource/idl__functions.c.em
// with input from ciotpf_messages:msg/CiotpfStringArray.idl
// generated code does not contain a copyright notice
#include "ciotpf_messages/msg/ciotpf_string_array__functions.h"

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>


// Include directives for member types
// Member `data`
#include "rosidl_generator_c/string_functions.h"

bool
ciotpf_messages__msg__CiotpfStringArray__init(ciotpf_messages__msg__CiotpfStringArray * msg)
{
  if (!msg) {
    return false;
  }
  // data
  if (!rosidl_generator_c__String__Sequence__init(&msg->data, 0)) {
    ciotpf_messages__msg__CiotpfStringArray__fini(msg);
    return false;
  }
  return true;
}

void
ciotpf_messages__msg__CiotpfStringArray__fini(ciotpf_messages__msg__CiotpfStringArray * msg)
{
  if (!msg) {
    return;
  }
  // data
  rosidl_generator_c__String__Sequence__fini(&msg->data);
}

ciotpf_messages__msg__CiotpfStringArray *
ciotpf_messages__msg__CiotpfStringArray__create()
{
  ciotpf_messages__msg__CiotpfStringArray * msg = (ciotpf_messages__msg__CiotpfStringArray *)malloc(sizeof(ciotpf_messages__msg__CiotpfStringArray));
  if (!msg) {
    return NULL;
  }
  memset(msg, 0, sizeof(ciotpf_messages__msg__CiotpfStringArray));
  bool success = ciotpf_messages__msg__CiotpfStringArray__init(msg);
  if (!success) {
    free(msg);
    return NULL;
  }
  return msg;
}

void
ciotpf_messages__msg__CiotpfStringArray__destroy(ciotpf_messages__msg__CiotpfStringArray * msg)
{
  if (msg) {
    ciotpf_messages__msg__CiotpfStringArray__fini(msg);
  }
  free(msg);
}


bool
ciotpf_messages__msg__CiotpfStringArray__Sequence__init(ciotpf_messages__msg__CiotpfStringArray__Sequence * array, size_t size)
{
  if (!array) {
    return false;
  }
  ciotpf_messages__msg__CiotpfStringArray * data = NULL;
  if (size) {
    data = (ciotpf_messages__msg__CiotpfStringArray *)calloc(size, sizeof(ciotpf_messages__msg__CiotpfStringArray));
    if (!data) {
      return false;
    }
    // initialize all array elements
    size_t i;
    for (i = 0; i < size; ++i) {
      bool success = ciotpf_messages__msg__CiotpfStringArray__init(&data[i]);
      if (!success) {
        break;
      }
    }
    if (i < size) {
      // if initialization failed finalize the already initialized array elements
      for (; i > 0; --i) {
        ciotpf_messages__msg__CiotpfStringArray__fini(&data[i - 1]);
      }
      free(data);
      return false;
    }
  }
  array->data = data;
  array->size = size;
  array->capacity = size;
  return true;
}

void
ciotpf_messages__msg__CiotpfStringArray__Sequence__fini(ciotpf_messages__msg__CiotpfStringArray__Sequence * array)
{
  if (!array) {
    return;
  }
  if (array->data) {
    // ensure that data and capacity values are consistent
    assert(array->capacity > 0);
    // finalize all array elements
    for (size_t i = 0; i < array->capacity; ++i) {
      ciotpf_messages__msg__CiotpfStringArray__fini(&array->data[i]);
    }
    free(array->data);
    array->data = NULL;
    array->size = 0;
    array->capacity = 0;
  } else {
    // ensure that data, size, and capacity values are consistent
    assert(0 == array->size);
    assert(0 == array->capacity);
  }
}

ciotpf_messages__msg__CiotpfStringArray__Sequence *
ciotpf_messages__msg__CiotpfStringArray__Sequence__create(size_t size)
{
  ciotpf_messages__msg__CiotpfStringArray__Sequence * array = (ciotpf_messages__msg__CiotpfStringArray__Sequence *)malloc(sizeof(ciotpf_messages__msg__CiotpfStringArray__Sequence));
  if (!array) {
    return NULL;
  }
  bool success = ciotpf_messages__msg__CiotpfStringArray__Sequence__init(array, size);
  if (!success) {
    free(array);
    return NULL;
  }
  return array;
}

void
ciotpf_messages__msg__CiotpfStringArray__Sequence__destroy(ciotpf_messages__msg__CiotpfStringArray__Sequence * array)
{
  if (array) {
    ciotpf_messages__msg__CiotpfStringArray__Sequence__fini(array);
  }
  free(array);
}
