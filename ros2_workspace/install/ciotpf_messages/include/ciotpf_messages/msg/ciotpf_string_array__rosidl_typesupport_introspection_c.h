// generated from rosidl_typesupport_introspection_c/resource/idl__rosidl_typesupport_introspection_c.h.em
// with input from ciotpf_messages:msg/CiotpfStringArray.idl
// generated code does not contain a copyright notice

#ifndef CIOTPF_MESSAGES__MSG__CIOTPF_STRING_ARRAY__ROSIDL_TYPESUPPORT_INTROSPECTION_C_H_
#define CIOTPF_MESSAGES__MSG__CIOTPF_STRING_ARRAY__ROSIDL_TYPESUPPORT_INTROSPECTION_C_H_

#ifdef __cplusplus
extern "C"
{
#endif


#include "rosidl_generator_c/message_type_support_struct.h"
#include "rosidl_typesupport_interface/macros.h"
#include "ciotpf_messages/msg/rosidl_typesupport_introspection_c__visibility_control.h"

ROSIDL_TYPESUPPORT_INTROSPECTION_C_PUBLIC_ciotpf_messages
const rosidl_message_type_support_t *
  ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, ciotpf_messages, msg, CiotpfStringArray)();

#ifdef __cplusplus
}
#endif

#endif  // CIOTPF_MESSAGES__MSG__CIOTPF_STRING_ARRAY__ROSIDL_TYPESUPPORT_INTROSPECTION_C_H_
