// generated from rosidl_generator_c/resource/idl.h.em
// with input from ciotpf_messages:msg/CiotpfAppNodeInfo.idl
// generated code does not contain a copyright notice

#ifndef CIOTPF_MESSAGES__MSG__CIOTPF_APP_NODE_INFO_H_
#define CIOTPF_MESSAGES__MSG__CIOTPF_APP_NODE_INFO_H_

#include "ciotpf_messages/msg/ciotpf_app_node_info__struct.h"
#include "ciotpf_messages/msg/ciotpf_app_node_info__functions.h"
#include "ciotpf_messages/msg/ciotpf_app_node_info__type_support.h"

#endif  // CIOTPF_MESSAGES__MSG__CIOTPF_APP_NODE_INFO_H_
