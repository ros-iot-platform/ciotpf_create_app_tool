// generated from rosidl_generator_cpp/resource/idl__struct.hpp.em
// with input from ciotpf_messages:msg/CiotpfStringArray.idl
// generated code does not contain a copyright notice

#ifndef CIOTPF_MESSAGES__MSG__CIOTPF_STRING_ARRAY__STRUCT_HPP_
#define CIOTPF_MESSAGES__MSG__CIOTPF_STRING_ARRAY__STRUCT_HPP_

#include <rosidl_generator_cpp/bounded_vector.hpp>
#include <rosidl_generator_cpp/message_initialization.hpp>
#include <algorithm>
#include <array>
#include <memory>
#include <string>
#include <vector>

// Protect against ERROR being predefined on Windows, in case somebody defines a
// constant by that name.
#if defined(_WIN32)
  #if defined(ERROR)
    #undef ERROR
  #endif
  #if defined(NO_ERROR)
    #undef NO_ERROR
  #endif
#endif

#ifndef _WIN32
# define DEPRECATED__ciotpf_messages__msg__CiotpfStringArray __attribute__((deprecated))
#else
# define DEPRECATED__ciotpf_messages__msg__CiotpfStringArray __declspec(deprecated)
#endif

namespace ciotpf_messages
{

namespace msg
{

// message struct
template<class ContainerAllocator>
struct CiotpfStringArray_
{
  using Type = CiotpfStringArray_<ContainerAllocator>;

  explicit CiotpfStringArray_(rosidl_generator_cpp::MessageInitialization _init = rosidl_generator_cpp::MessageInitialization::ALL)
  {
    (void)_init;
  }

  explicit CiotpfStringArray_(const ContainerAllocator & _alloc, rosidl_generator_cpp::MessageInitialization _init = rosidl_generator_cpp::MessageInitialization::ALL)
  {
    (void)_init;
    (void)_alloc;
  }

  // field types and members
  using _data_type =
    std::vector<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other>, typename ContainerAllocator::template rebind<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other>>::other>;
  _data_type data;

  // setters for named parameter idiom
  Type & set__data(
    const std::vector<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other>, typename ContainerAllocator::template rebind<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other>>::other> & _arg)
  {
    this->data = _arg;
    return *this;
  }

  // constant declarations

  // pointer types
  using RawPtr =
    ciotpf_messages::msg::CiotpfStringArray_<ContainerAllocator> *;
  using ConstRawPtr =
    const ciotpf_messages::msg::CiotpfStringArray_<ContainerAllocator> *;
  using SharedPtr =
    std::shared_ptr<ciotpf_messages::msg::CiotpfStringArray_<ContainerAllocator>>;
  using ConstSharedPtr =
    std::shared_ptr<ciotpf_messages::msg::CiotpfStringArray_<ContainerAllocator> const>;

  template<typename Deleter = std::default_delete<
      ciotpf_messages::msg::CiotpfStringArray_<ContainerAllocator>>>
  using UniquePtrWithDeleter =
    std::unique_ptr<ciotpf_messages::msg::CiotpfStringArray_<ContainerAllocator>, Deleter>;

  using UniquePtr = UniquePtrWithDeleter<>;

  template<typename Deleter = std::default_delete<
      ciotpf_messages::msg::CiotpfStringArray_<ContainerAllocator>>>
  using ConstUniquePtrWithDeleter =
    std::unique_ptr<ciotpf_messages::msg::CiotpfStringArray_<ContainerAllocator> const, Deleter>;
  using ConstUniquePtr = ConstUniquePtrWithDeleter<>;

  using WeakPtr =
    std::weak_ptr<ciotpf_messages::msg::CiotpfStringArray_<ContainerAllocator>>;
  using ConstWeakPtr =
    std::weak_ptr<ciotpf_messages::msg::CiotpfStringArray_<ContainerAllocator> const>;

  // pointer types similar to ROS 1, use SharedPtr / ConstSharedPtr instead
  // NOTE: Can't use 'using' here because GNU C++ can't parse attributes properly
  typedef DEPRECATED__ciotpf_messages__msg__CiotpfStringArray
    std::shared_ptr<ciotpf_messages::msg::CiotpfStringArray_<ContainerAllocator>>
    Ptr;
  typedef DEPRECATED__ciotpf_messages__msg__CiotpfStringArray
    std::shared_ptr<ciotpf_messages::msg::CiotpfStringArray_<ContainerAllocator> const>
    ConstPtr;

  // comparison operators
  bool operator==(const CiotpfStringArray_ & other) const
  {
    if (this->data != other.data) {
      return false;
    }
    return true;
  }
  bool operator!=(const CiotpfStringArray_ & other) const
  {
    return !this->operator==(other);
  }
};  // struct CiotpfStringArray_

// alias to use template instance with default allocator
using CiotpfStringArray =
  ciotpf_messages::msg::CiotpfStringArray_<std::allocator<void>>;

// constant definitions

}  // namespace msg

}  // namespace ciotpf_messages

#endif  // CIOTPF_MESSAGES__MSG__CIOTPF_STRING_ARRAY__STRUCT_HPP_
