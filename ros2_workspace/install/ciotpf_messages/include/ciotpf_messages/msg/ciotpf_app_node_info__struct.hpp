// generated from rosidl_generator_cpp/resource/idl__struct.hpp.em
// with input from ciotpf_messages:msg/CiotpfAppNodeInfo.idl
// generated code does not contain a copyright notice

#ifndef CIOTPF_MESSAGES__MSG__CIOTPF_APP_NODE_INFO__STRUCT_HPP_
#define CIOTPF_MESSAGES__MSG__CIOTPF_APP_NODE_INFO__STRUCT_HPP_

#include <rosidl_generator_cpp/bounded_vector.hpp>
#include <rosidl_generator_cpp/message_initialization.hpp>
#include <algorithm>
#include <array>
#include <memory>
#include <string>
#include <vector>

// Protect against ERROR being predefined on Windows, in case somebody defines a
// constant by that name.
#if defined(_WIN32)
  #if defined(ERROR)
    #undef ERROR
  #endif
  #if defined(NO_ERROR)
    #undef NO_ERROR
  #endif
#endif

// Include directives for member types
// Member 'subscriptions'
// Member 'publishers'
#include "ciotpf_messages/msg/ciotpf_manual_topic__struct.hpp"

#ifndef _WIN32
# define DEPRECATED__ciotpf_messages__msg__CiotpfAppNodeInfo __attribute__((deprecated))
#else
# define DEPRECATED__ciotpf_messages__msg__CiotpfAppNodeInfo __declspec(deprecated)
#endif

namespace ciotpf_messages
{

namespace msg
{

// message struct
template<class ContainerAllocator>
struct CiotpfAppNodeInfo_
{
  using Type = CiotpfAppNodeInfo_<ContainerAllocator>;

  explicit CiotpfAppNodeInfo_(rosidl_generator_cpp::MessageInitialization _init = rosidl_generator_cpp::MessageInitialization::ALL)
  {
    (void)_init;
  }

  explicit CiotpfAppNodeInfo_(const ContainerAllocator & _alloc, rosidl_generator_cpp::MessageInitialization _init = rosidl_generator_cpp::MessageInitialization::ALL)
  {
    (void)_init;
    (void)_alloc;
  }

  // field types and members
  using _subscriptions_type =
    std::vector<ciotpf_messages::msg::CiotpfManualTopic_<ContainerAllocator>, typename ContainerAllocator::template rebind<ciotpf_messages::msg::CiotpfManualTopic_<ContainerAllocator>>::other>;
  _subscriptions_type subscriptions;
  using _publishers_type =
    std::vector<ciotpf_messages::msg::CiotpfManualTopic_<ContainerAllocator>, typename ContainerAllocator::template rebind<ciotpf_messages::msg::CiotpfManualTopic_<ContainerAllocator>>::other>;
  _publishers_type publishers;

  // setters for named parameter idiom
  Type & set__subscriptions(
    const std::vector<ciotpf_messages::msg::CiotpfManualTopic_<ContainerAllocator>, typename ContainerAllocator::template rebind<ciotpf_messages::msg::CiotpfManualTopic_<ContainerAllocator>>::other> & _arg)
  {
    this->subscriptions = _arg;
    return *this;
  }
  Type & set__publishers(
    const std::vector<ciotpf_messages::msg::CiotpfManualTopic_<ContainerAllocator>, typename ContainerAllocator::template rebind<ciotpf_messages::msg::CiotpfManualTopic_<ContainerAllocator>>::other> & _arg)
  {
    this->publishers = _arg;
    return *this;
  }

  // constant declarations

  // pointer types
  using RawPtr =
    ciotpf_messages::msg::CiotpfAppNodeInfo_<ContainerAllocator> *;
  using ConstRawPtr =
    const ciotpf_messages::msg::CiotpfAppNodeInfo_<ContainerAllocator> *;
  using SharedPtr =
    std::shared_ptr<ciotpf_messages::msg::CiotpfAppNodeInfo_<ContainerAllocator>>;
  using ConstSharedPtr =
    std::shared_ptr<ciotpf_messages::msg::CiotpfAppNodeInfo_<ContainerAllocator> const>;

  template<typename Deleter = std::default_delete<
      ciotpf_messages::msg::CiotpfAppNodeInfo_<ContainerAllocator>>>
  using UniquePtrWithDeleter =
    std::unique_ptr<ciotpf_messages::msg::CiotpfAppNodeInfo_<ContainerAllocator>, Deleter>;

  using UniquePtr = UniquePtrWithDeleter<>;

  template<typename Deleter = std::default_delete<
      ciotpf_messages::msg::CiotpfAppNodeInfo_<ContainerAllocator>>>
  using ConstUniquePtrWithDeleter =
    std::unique_ptr<ciotpf_messages::msg::CiotpfAppNodeInfo_<ContainerAllocator> const, Deleter>;
  using ConstUniquePtr = ConstUniquePtrWithDeleter<>;

  using WeakPtr =
    std::weak_ptr<ciotpf_messages::msg::CiotpfAppNodeInfo_<ContainerAllocator>>;
  using ConstWeakPtr =
    std::weak_ptr<ciotpf_messages::msg::CiotpfAppNodeInfo_<ContainerAllocator> const>;

  // pointer types similar to ROS 1, use SharedPtr / ConstSharedPtr instead
  // NOTE: Can't use 'using' here because GNU C++ can't parse attributes properly
  typedef DEPRECATED__ciotpf_messages__msg__CiotpfAppNodeInfo
    std::shared_ptr<ciotpf_messages::msg::CiotpfAppNodeInfo_<ContainerAllocator>>
    Ptr;
  typedef DEPRECATED__ciotpf_messages__msg__CiotpfAppNodeInfo
    std::shared_ptr<ciotpf_messages::msg::CiotpfAppNodeInfo_<ContainerAllocator> const>
    ConstPtr;

  // comparison operators
  bool operator==(const CiotpfAppNodeInfo_ & other) const
  {
    if (this->subscriptions != other.subscriptions) {
      return false;
    }
    if (this->publishers != other.publishers) {
      return false;
    }
    return true;
  }
  bool operator!=(const CiotpfAppNodeInfo_ & other) const
  {
    return !this->operator==(other);
  }
};  // struct CiotpfAppNodeInfo_

// alias to use template instance with default allocator
using CiotpfAppNodeInfo =
  ciotpf_messages::msg::CiotpfAppNodeInfo_<std::allocator<void>>;

// constant definitions

}  // namespace msg

}  // namespace ciotpf_messages

#endif  // CIOTPF_MESSAGES__MSG__CIOTPF_APP_NODE_INFO__STRUCT_HPP_
