// generated from rosidl_generator_cpp/resource/idl__traits.hpp.em
// with input from ciotpf_messages:msg/CiotpfManualTopic.idl
// generated code does not contain a copyright notice

#ifndef CIOTPF_MESSAGES__MSG__CIOTPF_MANUAL_TOPIC__TRAITS_HPP_
#define CIOTPF_MESSAGES__MSG__CIOTPF_MANUAL_TOPIC__TRAITS_HPP_

#include "ciotpf_messages/msg/ciotpf_manual_topic__struct.hpp"
#include <rosidl_generator_cpp/traits.hpp>
#include <stdint.h>
#include <type_traits>

// Include directives for member types
// Member 'topic_names'
#include "ciotpf_messages/msg/ciotpf_string_array__traits.hpp"

namespace rosidl_generator_traits
{

template<>
inline const char * data_type<ciotpf_messages::msg::CiotpfManualTopic>()
{
  return "ciotpf_messages::msg::CiotpfManualTopic";
}

template<>
struct has_fixed_size<ciotpf_messages::msg::CiotpfManualTopic>
  : std::integral_constant<bool, false> {};

template<>
struct has_bounded_size<ciotpf_messages::msg::CiotpfManualTopic>
  : std::integral_constant<bool, false> {};

}  // namespace rosidl_generator_traits

#endif  // CIOTPF_MESSAGES__MSG__CIOTPF_MANUAL_TOPIC__TRAITS_HPP_
