// generated from rosidl_generator_c/resource/idl__struct.h.em
// with input from ciotpf_messages:msg/CiotpfManualTopic.idl
// generated code does not contain a copyright notice

#ifndef CIOTPF_MESSAGES__MSG__CIOTPF_MANUAL_TOPIC__FUNCTIONS_H_
#define CIOTPF_MESSAGES__MSG__CIOTPF_MANUAL_TOPIC__FUNCTIONS_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>
#include <stdlib.h>

#include "rosidl_generator_c/visibility_control.h"
#include "ciotpf_messages/msg/rosidl_generator_c__visibility_control.h"

#include "ciotpf_messages/msg/ciotpf_manual_topic__struct.h"

/// Initialize msg/CiotpfManualTopic message.
/**
 * If the init function is called twice for the same message without
 * calling fini inbetween previously allocated memory will be leaked.
 * \param[in,out] msg The previously allocated message pointer.
 * Fields without a default value will not be initialized by this function.
 * You might want to call memset(msg, 0, sizeof(
 * ciotpf_messages__msg__CiotpfManualTopic
 * )) before or use
 * ciotpf_messages__msg__CiotpfManualTopic__create()
 * to allocate and initialize the message.
 * \return true if initialization was successful, otherwise false
 */
ROSIDL_GENERATOR_C_PUBLIC_ciotpf_messages
bool
ciotpf_messages__msg__CiotpfManualTopic__init(ciotpf_messages__msg__CiotpfManualTopic * msg);

/// Finalize msg/CiotpfManualTopic message.
/**
 * \param[in,out] msg The allocated message pointer.
 */
ROSIDL_GENERATOR_C_PUBLIC_ciotpf_messages
void
ciotpf_messages__msg__CiotpfManualTopic__fini(ciotpf_messages__msg__CiotpfManualTopic * msg);

/// Create msg/CiotpfManualTopic message.
/**
 * It allocates the memory for the message, sets the memory to zero, and
 * calls
 * ciotpf_messages__msg__CiotpfManualTopic__init().
 * \return The pointer to the initialized message if successful,
 * otherwise NULL
 */
ROSIDL_GENERATOR_C_PUBLIC_ciotpf_messages
ciotpf_messages__msg__CiotpfManualTopic *
ciotpf_messages__msg__CiotpfManualTopic__create();

/// Destroy msg/CiotpfManualTopic message.
/**
 * It calls
 * ciotpf_messages__msg__CiotpfManualTopic__fini()
 * and frees the memory of the message.
 * \param[in,out] msg The allocated message pointer.
 */
ROSIDL_GENERATOR_C_PUBLIC_ciotpf_messages
void
ciotpf_messages__msg__CiotpfManualTopic__destroy(ciotpf_messages__msg__CiotpfManualTopic * msg);


/// Initialize array of msg/CiotpfManualTopic messages.
/**
 * It allocates the memory for the number of elements and calls
 * ciotpf_messages__msg__CiotpfManualTopic__init()
 * for each element of the array.
 * \param[in,out] array The allocated array pointer.
 * \param[in] size The size / capacity of the array.
 * \return true if initialization was successful, otherwise false
 * If the array pointer is valid and the size is zero it is guaranteed
 # to return true.
 */
ROSIDL_GENERATOR_C_PUBLIC_ciotpf_messages
bool
ciotpf_messages__msg__CiotpfManualTopic__Sequence__init(ciotpf_messages__msg__CiotpfManualTopic__Sequence * array, size_t size);

/// Finalize array of msg/CiotpfManualTopic messages.
/**
 * It calls
 * ciotpf_messages__msg__CiotpfManualTopic__fini()
 * for each element of the array and frees the memory for the number of
 * elements.
 * \param[in,out] array The initialized array pointer.
 */
ROSIDL_GENERATOR_C_PUBLIC_ciotpf_messages
void
ciotpf_messages__msg__CiotpfManualTopic__Sequence__fini(ciotpf_messages__msg__CiotpfManualTopic__Sequence * array);

/// Create array of msg/CiotpfManualTopic messages.
/**
 * It allocates the memory for the array and calls
 * ciotpf_messages__msg__CiotpfManualTopic__Sequence__init().
 * \param[in] size The size / capacity of the array.
 * \return The pointer to the initialized array if successful, otherwise NULL
 */
ROSIDL_GENERATOR_C_PUBLIC_ciotpf_messages
ciotpf_messages__msg__CiotpfManualTopic__Sequence *
ciotpf_messages__msg__CiotpfManualTopic__Sequence__create(size_t size);

/// Destroy array of msg/CiotpfManualTopic messages.
/**
 * It calls
 * ciotpf_messages__msg__CiotpfManualTopic__Sequence__fini()
 * on the array,
 * and frees the memory of the array.
 * \param[in,out] array The initialized array pointer.
 */
ROSIDL_GENERATOR_C_PUBLIC_ciotpf_messages
void
ciotpf_messages__msg__CiotpfManualTopic__Sequence__destroy(ciotpf_messages__msg__CiotpfManualTopic__Sequence * array);

#ifdef __cplusplus
}
#endif

#endif  // CIOTPF_MESSAGES__MSG__CIOTPF_MANUAL_TOPIC__FUNCTIONS_H_
