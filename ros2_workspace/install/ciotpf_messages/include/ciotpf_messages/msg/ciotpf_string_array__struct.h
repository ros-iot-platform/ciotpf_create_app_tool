// generated from rosidl_generator_c/resource/idl__struct.h.em
// with input from ciotpf_messages:msg/CiotpfStringArray.idl
// generated code does not contain a copyright notice

#ifndef CIOTPF_MESSAGES__MSG__CIOTPF_STRING_ARRAY__STRUCT_H_
#define CIOTPF_MESSAGES__MSG__CIOTPF_STRING_ARRAY__STRUCT_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>


// Constants defined in the message

// Include directives for member types
// Member 'data'
#include "rosidl_generator_c/string.h"

// Struct defined in msg/CiotpfStringArray in the package ciotpf_messages.
typedef struct ciotpf_messages__msg__CiotpfStringArray
{
  rosidl_generator_c__String__Sequence data;
} ciotpf_messages__msg__CiotpfStringArray;

// Struct for a sequence of ciotpf_messages__msg__CiotpfStringArray.
typedef struct ciotpf_messages__msg__CiotpfStringArray__Sequence
{
  ciotpf_messages__msg__CiotpfStringArray * data;
  /// The number of valid items in data
  size_t size;
  /// The number of allocated items in data
  size_t capacity;
} ciotpf_messages__msg__CiotpfStringArray__Sequence;

#ifdef __cplusplus
}
#endif

#endif  // CIOTPF_MESSAGES__MSG__CIOTPF_STRING_ARRAY__STRUCT_H_
