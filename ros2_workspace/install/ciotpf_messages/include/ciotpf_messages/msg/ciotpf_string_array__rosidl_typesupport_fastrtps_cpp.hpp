// generated from rosidl_typesupport_fastrtps_cpp/resource/idl__rosidl_typesupport_fastrtps_cpp.hpp.em
// with input from ciotpf_messages:msg/CiotpfStringArray.idl
// generated code does not contain a copyright notice

#ifndef CIOTPF_MESSAGES__MSG__CIOTPF_STRING_ARRAY__ROSIDL_TYPESUPPORT_FASTRTPS_CPP_HPP_
#define CIOTPF_MESSAGES__MSG__CIOTPF_STRING_ARRAY__ROSIDL_TYPESUPPORT_FASTRTPS_CPP_HPP_

#include "rosidl_generator_c/message_type_support_struct.h"
#include "rosidl_typesupport_interface/macros.h"
#include "ciotpf_messages/msg/rosidl_typesupport_fastrtps_cpp__visibility_control.h"
#include "ciotpf_messages/msg/ciotpf_string_array__struct.hpp"

#ifndef _WIN32
# pragma GCC diagnostic push
# pragma GCC diagnostic ignored "-Wunused-parameter"
# ifdef __clang__
#  pragma clang diagnostic ignored "-Wdeprecated-register"
#  pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
# endif
#endif
#ifndef _WIN32
# pragma GCC diagnostic pop
#endif

#include "fastcdr/Cdr.h"

namespace ciotpf_messages
{

namespace msg
{

namespace typesupport_fastrtps_cpp
{

bool
ROSIDL_TYPESUPPORT_FASTRTPS_CPP_PUBLIC_ciotpf_messages
cdr_serialize(
  const ciotpf_messages::msg::CiotpfStringArray & ros_message,
  eprosima::fastcdr::Cdr & cdr);

bool
ROSIDL_TYPESUPPORT_FASTRTPS_CPP_PUBLIC_ciotpf_messages
cdr_deserialize(
  eprosima::fastcdr::Cdr & cdr,
  ciotpf_messages::msg::CiotpfStringArray & ros_message);

size_t
ROSIDL_TYPESUPPORT_FASTRTPS_CPP_PUBLIC_ciotpf_messages
get_serialized_size(
  const ciotpf_messages::msg::CiotpfStringArray & ros_message,
  size_t current_alignment);

size_t
ROSIDL_TYPESUPPORT_FASTRTPS_CPP_PUBLIC_ciotpf_messages
max_serialized_size_CiotpfStringArray(
  bool & full_bounded,
  size_t current_alignment);

}  // namespace typesupport_fastrtps_cpp

}  // namespace msg

}  // namespace ciotpf_messages

#ifdef __cplusplus
extern "C"
{
#endif

ROSIDL_TYPESUPPORT_FASTRTPS_CPP_PUBLIC_ciotpf_messages
const rosidl_message_type_support_t *
  ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_fastrtps_cpp, ciotpf_messages, msg, CiotpfStringArray)();

#ifdef __cplusplus
}
#endif

#endif  // CIOTPF_MESSAGES__MSG__CIOTPF_STRING_ARRAY__ROSIDL_TYPESUPPORT_FASTRTPS_CPP_HPP_
