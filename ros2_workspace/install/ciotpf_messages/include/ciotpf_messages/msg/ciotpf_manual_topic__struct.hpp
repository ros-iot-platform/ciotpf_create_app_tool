// generated from rosidl_generator_cpp/resource/idl__struct.hpp.em
// with input from ciotpf_messages:msg/CiotpfManualTopic.idl
// generated code does not contain a copyright notice

#ifndef CIOTPF_MESSAGES__MSG__CIOTPF_MANUAL_TOPIC__STRUCT_HPP_
#define CIOTPF_MESSAGES__MSG__CIOTPF_MANUAL_TOPIC__STRUCT_HPP_

#include <rosidl_generator_cpp/bounded_vector.hpp>
#include <rosidl_generator_cpp/message_initialization.hpp>
#include <algorithm>
#include <array>
#include <memory>
#include <string>
#include <vector>

// Protect against ERROR being predefined on Windows, in case somebody defines a
// constant by that name.
#if defined(_WIN32)
  #if defined(ERROR)
    #undef ERROR
  #endif
  #if defined(NO_ERROR)
    #undef NO_ERROR
  #endif
#endif

// Include directives for member types
// Member 'topic_names'
#include "ciotpf_messages/msg/ciotpf_string_array__struct.hpp"

#ifndef _WIN32
# define DEPRECATED__ciotpf_messages__msg__CiotpfManualTopic __attribute__((deprecated))
#else
# define DEPRECATED__ciotpf_messages__msg__CiotpfManualTopic __declspec(deprecated)
#endif

namespace ciotpf_messages
{

namespace msg
{

// message struct
template<class ContainerAllocator>
struct CiotpfManualTopic_
{
  using Type = CiotpfManualTopic_<ContainerAllocator>;

  explicit CiotpfManualTopic_(rosidl_generator_cpp::MessageInitialization _init = rosidl_generator_cpp::MessageInitialization::ALL)
  : topic_names(_init)
  {
    if (rosidl_generator_cpp::MessageInitialization::ALL == _init ||
      rosidl_generator_cpp::MessageInitialization::ZERO == _init)
    {
      this->name = "";
      this->topic_type = "";
    }
  }

  explicit CiotpfManualTopic_(const ContainerAllocator & _alloc, rosidl_generator_cpp::MessageInitialization _init = rosidl_generator_cpp::MessageInitialization::ALL)
  : name(_alloc),
    topic_type(_alloc),
    topic_names(_alloc, _init)
  {
    if (rosidl_generator_cpp::MessageInitialization::ALL == _init ||
      rosidl_generator_cpp::MessageInitialization::ZERO == _init)
    {
      this->name = "";
      this->topic_type = "";
    }
  }

  // field types and members
  using _name_type =
    std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other>;
  _name_type name;
  using _topic_type_type =
    std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other>;
  _topic_type_type topic_type;
  using _topic_names_type =
    ciotpf_messages::msg::CiotpfStringArray_<ContainerAllocator>;
  _topic_names_type topic_names;

  // setters for named parameter idiom
  Type & set__name(
    const std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other> & _arg)
  {
    this->name = _arg;
    return *this;
  }
  Type & set__topic_type(
    const std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other> & _arg)
  {
    this->topic_type = _arg;
    return *this;
  }
  Type & set__topic_names(
    const ciotpf_messages::msg::CiotpfStringArray_<ContainerAllocator> & _arg)
  {
    this->topic_names = _arg;
    return *this;
  }

  // constant declarations

  // pointer types
  using RawPtr =
    ciotpf_messages::msg::CiotpfManualTopic_<ContainerAllocator> *;
  using ConstRawPtr =
    const ciotpf_messages::msg::CiotpfManualTopic_<ContainerAllocator> *;
  using SharedPtr =
    std::shared_ptr<ciotpf_messages::msg::CiotpfManualTopic_<ContainerAllocator>>;
  using ConstSharedPtr =
    std::shared_ptr<ciotpf_messages::msg::CiotpfManualTopic_<ContainerAllocator> const>;

  template<typename Deleter = std::default_delete<
      ciotpf_messages::msg::CiotpfManualTopic_<ContainerAllocator>>>
  using UniquePtrWithDeleter =
    std::unique_ptr<ciotpf_messages::msg::CiotpfManualTopic_<ContainerAllocator>, Deleter>;

  using UniquePtr = UniquePtrWithDeleter<>;

  template<typename Deleter = std::default_delete<
      ciotpf_messages::msg::CiotpfManualTopic_<ContainerAllocator>>>
  using ConstUniquePtrWithDeleter =
    std::unique_ptr<ciotpf_messages::msg::CiotpfManualTopic_<ContainerAllocator> const, Deleter>;
  using ConstUniquePtr = ConstUniquePtrWithDeleter<>;

  using WeakPtr =
    std::weak_ptr<ciotpf_messages::msg::CiotpfManualTopic_<ContainerAllocator>>;
  using ConstWeakPtr =
    std::weak_ptr<ciotpf_messages::msg::CiotpfManualTopic_<ContainerAllocator> const>;

  // pointer types similar to ROS 1, use SharedPtr / ConstSharedPtr instead
  // NOTE: Can't use 'using' here because GNU C++ can't parse attributes properly
  typedef DEPRECATED__ciotpf_messages__msg__CiotpfManualTopic
    std::shared_ptr<ciotpf_messages::msg::CiotpfManualTopic_<ContainerAllocator>>
    Ptr;
  typedef DEPRECATED__ciotpf_messages__msg__CiotpfManualTopic
    std::shared_ptr<ciotpf_messages::msg::CiotpfManualTopic_<ContainerAllocator> const>
    ConstPtr;

  // comparison operators
  bool operator==(const CiotpfManualTopic_ & other) const
  {
    if (this->name != other.name) {
      return false;
    }
    if (this->topic_type != other.topic_type) {
      return false;
    }
    if (this->topic_names != other.topic_names) {
      return false;
    }
    return true;
  }
  bool operator!=(const CiotpfManualTopic_ & other) const
  {
    return !this->operator==(other);
  }
};  // struct CiotpfManualTopic_

// alias to use template instance with default allocator
using CiotpfManualTopic =
  ciotpf_messages::msg::CiotpfManualTopic_<std::allocator<void>>;

// constant definitions

}  // namespace msg

}  // namespace ciotpf_messages

#endif  // CIOTPF_MESSAGES__MSG__CIOTPF_MANUAL_TOPIC__STRUCT_HPP_
