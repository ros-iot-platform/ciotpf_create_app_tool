// generated from rosidl_typesupport_fastrtps_c/resource/idl__rosidl_typesupport_fastrtps_c.h.em
// with input from ciotpf_messages:msg/CiotpfManualTopic.idl
// generated code does not contain a copyright notice
#ifndef CIOTPF_MESSAGES__MSG__CIOTPF_MANUAL_TOPIC__ROSIDL_TYPESUPPORT_FASTRTPS_C_H_
#define CIOTPF_MESSAGES__MSG__CIOTPF_MANUAL_TOPIC__ROSIDL_TYPESUPPORT_FASTRTPS_C_H_


#include <stddef.h>
#include "rosidl_generator_c/message_type_support_struct.h"
#include "rosidl_typesupport_interface/macros.h"
#include "ciotpf_messages/msg/rosidl_typesupport_fastrtps_c__visibility_control.h"

#ifdef __cplusplus
extern "C"
{
#endif

ROSIDL_TYPESUPPORT_FASTRTPS_C_PUBLIC_ciotpf_messages
size_t get_serialized_size_ciotpf_messages__msg__CiotpfManualTopic(
  const void * untyped_ros_message,
  size_t current_alignment);

ROSIDL_TYPESUPPORT_FASTRTPS_C_PUBLIC_ciotpf_messages
size_t max_serialized_size_ciotpf_messages__msg__CiotpfManualTopic(
  bool & full_bounded,
  size_t current_alignment);

ROSIDL_TYPESUPPORT_FASTRTPS_C_PUBLIC_ciotpf_messages
const rosidl_message_type_support_t *
  ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_fastrtps_c, ciotpf_messages, msg, CiotpfManualTopic)();

#ifdef __cplusplus
}
#endif

#endif  // CIOTPF_MESSAGES__MSG__CIOTPF_MANUAL_TOPIC__ROSIDL_TYPESUPPORT_FASTRTPS_C_H_
