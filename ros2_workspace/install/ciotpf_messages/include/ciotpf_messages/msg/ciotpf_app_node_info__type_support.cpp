// generated from rosidl_typesupport_introspection_cpp/resource/idl__type_support.cpp.em
// with input from ciotpf_messages:msg/CiotpfAppNodeInfo.idl
// generated code does not contain a copyright notice

#include "array"
#include "cstddef"
#include "string"
#include "vector"
#include "rosidl_generator_c/message_type_support_struct.h"
#include "rosidl_typesupport_cpp/message_type_support.hpp"
#include "rosidl_typesupport_interface/macros.h"
#include "ciotpf_messages/msg/ciotpf_app_node_info__struct.hpp"
#include "rosidl_typesupport_introspection_cpp/field_types.hpp"
#include "rosidl_typesupport_introspection_cpp/identifier.hpp"
#include "rosidl_typesupport_introspection_cpp/message_introspection.hpp"
#include "rosidl_typesupport_introspection_cpp/message_type_support_decl.hpp"
#include "rosidl_typesupport_introspection_cpp/visibility_control.h"

namespace ciotpf_messages
{

namespace msg
{

namespace rosidl_typesupport_introspection_cpp
{

size_t size_function__CiotpfAppNodeInfo__subscriptions(const void * untyped_member)
{
  const auto * member = reinterpret_cast<const std::vector<ciotpf_messages::msg::CiotpfManualTopic> *>(untyped_member);
  return member->size();
}

const void * get_const_function__CiotpfAppNodeInfo__subscriptions(const void * untyped_member, size_t index)
{
  const auto & member =
    *reinterpret_cast<const std::vector<ciotpf_messages::msg::CiotpfManualTopic> *>(untyped_member);
  return &member[index];
}

void * get_function__CiotpfAppNodeInfo__subscriptions(void * untyped_member, size_t index)
{
  auto & member =
    *reinterpret_cast<std::vector<ciotpf_messages::msg::CiotpfManualTopic> *>(untyped_member);
  return &member[index];
}

void resize_function__CiotpfAppNodeInfo__subscriptions(void * untyped_member, size_t size)
{
  auto * member =
    reinterpret_cast<std::vector<ciotpf_messages::msg::CiotpfManualTopic> *>(untyped_member);
  member->resize(size);
}

size_t size_function__CiotpfAppNodeInfo__publishers(const void * untyped_member)
{
  const auto * member = reinterpret_cast<const std::vector<ciotpf_messages::msg::CiotpfManualTopic> *>(untyped_member);
  return member->size();
}

const void * get_const_function__CiotpfAppNodeInfo__publishers(const void * untyped_member, size_t index)
{
  const auto & member =
    *reinterpret_cast<const std::vector<ciotpf_messages::msg::CiotpfManualTopic> *>(untyped_member);
  return &member[index];
}

void * get_function__CiotpfAppNodeInfo__publishers(void * untyped_member, size_t index)
{
  auto & member =
    *reinterpret_cast<std::vector<ciotpf_messages::msg::CiotpfManualTopic> *>(untyped_member);
  return &member[index];
}

void resize_function__CiotpfAppNodeInfo__publishers(void * untyped_member, size_t size)
{
  auto * member =
    reinterpret_cast<std::vector<ciotpf_messages::msg::CiotpfManualTopic> *>(untyped_member);
  member->resize(size);
}

static const ::rosidl_typesupport_introspection_cpp::MessageMember CiotpfAppNodeInfo_message_member_array[2] = {
  {
    "subscriptions",  // name
    ::rosidl_typesupport_introspection_cpp::ROS_TYPE_MESSAGE,  // type
    0,  // upper bound of string
    ::rosidl_typesupport_introspection_cpp::get_message_type_support_handle<ciotpf_messages::msg::CiotpfManualTopic>(),  // members of sub message
    true,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(ciotpf_messages::msg::CiotpfAppNodeInfo, subscriptions),  // bytes offset in struct
    nullptr,  // default value
    size_function__CiotpfAppNodeInfo__subscriptions,  // size() function pointer
    get_const_function__CiotpfAppNodeInfo__subscriptions,  // get_const(index) function pointer
    get_function__CiotpfAppNodeInfo__subscriptions,  // get(index) function pointer
    resize_function__CiotpfAppNodeInfo__subscriptions  // resize(index) function pointer
  },
  {
    "publishers",  // name
    ::rosidl_typesupport_introspection_cpp::ROS_TYPE_MESSAGE,  // type
    0,  // upper bound of string
    ::rosidl_typesupport_introspection_cpp::get_message_type_support_handle<ciotpf_messages::msg::CiotpfManualTopic>(),  // members of sub message
    true,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(ciotpf_messages::msg::CiotpfAppNodeInfo, publishers),  // bytes offset in struct
    nullptr,  // default value
    size_function__CiotpfAppNodeInfo__publishers,  // size() function pointer
    get_const_function__CiotpfAppNodeInfo__publishers,  // get_const(index) function pointer
    get_function__CiotpfAppNodeInfo__publishers,  // get(index) function pointer
    resize_function__CiotpfAppNodeInfo__publishers  // resize(index) function pointer
  }
};

static const ::rosidl_typesupport_introspection_cpp::MessageMembers CiotpfAppNodeInfo_message_members = {
  "ciotpf_messages::msg",  // message namespace
  "CiotpfAppNodeInfo",  // message name
  2,  // number of fields
  sizeof(ciotpf_messages::msg::CiotpfAppNodeInfo),
  CiotpfAppNodeInfo_message_member_array  // message members
};

static const rosidl_message_type_support_t CiotpfAppNodeInfo_message_type_support_handle = {
  ::rosidl_typesupport_introspection_cpp::typesupport_identifier,
  &CiotpfAppNodeInfo_message_members,
  get_message_typesupport_handle_function,
};

}  // namespace rosidl_typesupport_introspection_cpp

}  // namespace msg

}  // namespace ciotpf_messages


namespace rosidl_typesupport_introspection_cpp
{

template<>
ROSIDL_TYPESUPPORT_INTROSPECTION_CPP_PUBLIC
const rosidl_message_type_support_t *
get_message_type_support_handle<ciotpf_messages::msg::CiotpfAppNodeInfo>()
{
  return &::ciotpf_messages::msg::rosidl_typesupport_introspection_cpp::CiotpfAppNodeInfo_message_type_support_handle;
}

}  // namespace rosidl_typesupport_introspection_cpp

#ifdef __cplusplus
extern "C"
{
#endif

ROSIDL_TYPESUPPORT_INTROSPECTION_CPP_PUBLIC
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_cpp, ciotpf_messages, msg, CiotpfAppNodeInfo)() {
  return &::ciotpf_messages::msg::rosidl_typesupport_introspection_cpp::CiotpfAppNodeInfo_message_type_support_handle;
}

#ifdef __cplusplus
}
#endif
