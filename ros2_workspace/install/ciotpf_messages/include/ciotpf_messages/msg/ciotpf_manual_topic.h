// generated from rosidl_generator_c/resource/idl.h.em
// with input from ciotpf_messages:msg/CiotpfManualTopic.idl
// generated code does not contain a copyright notice

#ifndef CIOTPF_MESSAGES__MSG__CIOTPF_MANUAL_TOPIC_H_
#define CIOTPF_MESSAGES__MSG__CIOTPF_MANUAL_TOPIC_H_

#include "ciotpf_messages/msg/ciotpf_manual_topic__struct.h"
#include "ciotpf_messages/msg/ciotpf_manual_topic__functions.h"
#include "ciotpf_messages/msg/ciotpf_manual_topic__type_support.h"

#endif  // CIOTPF_MESSAGES__MSG__CIOTPF_MANUAL_TOPIC_H_
