// generated from rosidl_generator_cpp/resource/idl__traits.hpp.em
// with input from ciotpf_messages:srv/CiotpfAppNodeManualTopicConfig.idl
// generated code does not contain a copyright notice

#ifndef CIOTPF_MESSAGES__SRV__CIOTPF_APP_NODE_MANUAL_TOPIC_CONFIG__TRAITS_HPP_
#define CIOTPF_MESSAGES__SRV__CIOTPF_APP_NODE_MANUAL_TOPIC_CONFIG__TRAITS_HPP_

#include "ciotpf_messages/srv/ciotpf_app_node_manual_topic_config__struct.hpp"
#include <rosidl_generator_cpp/traits.hpp>
#include <stdint.h>
#include <type_traits>

namespace rosidl_generator_traits
{

template<>
inline const char * data_type<ciotpf_messages::srv::CiotpfAppNodeManualTopicConfig_Request>()
{
  return "ciotpf_messages::srv::CiotpfAppNodeManualTopicConfig_Request";
}

template<>
struct has_fixed_size<ciotpf_messages::srv::CiotpfAppNodeManualTopicConfig_Request>
  : std::integral_constant<bool, false> {};

template<>
struct has_bounded_size<ciotpf_messages::srv::CiotpfAppNodeManualTopicConfig_Request>
  : std::integral_constant<bool, false> {};

}  // namespace rosidl_generator_traits

namespace rosidl_generator_traits
{

template<>
inline const char * data_type<ciotpf_messages::srv::CiotpfAppNodeManualTopicConfig_Response>()
{
  return "ciotpf_messages::srv::CiotpfAppNodeManualTopicConfig_Response";
}

template<>
struct has_fixed_size<ciotpf_messages::srv::CiotpfAppNodeManualTopicConfig_Response>
  : std::integral_constant<bool, false> {};

template<>
struct has_bounded_size<ciotpf_messages::srv::CiotpfAppNodeManualTopicConfig_Response>
  : std::integral_constant<bool, false> {};

}  // namespace rosidl_generator_traits

namespace rosidl_generator_traits
{

template<>
inline const char * data_type<ciotpf_messages::srv::CiotpfAppNodeManualTopicConfig>()
{
  return "ciotpf_messages::srv::CiotpfAppNodeManualTopicConfig";
}

template<>
struct has_fixed_size<ciotpf_messages::srv::CiotpfAppNodeManualTopicConfig>
  : std::integral_constant<
    bool,
    has_fixed_size<ciotpf_messages::srv::CiotpfAppNodeManualTopicConfig_Request>::value &&
    has_fixed_size<ciotpf_messages::srv::CiotpfAppNodeManualTopicConfig_Response>::value
  >
{
};

template<>
struct has_bounded_size<ciotpf_messages::srv::CiotpfAppNodeManualTopicConfig>
  : std::integral_constant<
    bool,
    has_bounded_size<ciotpf_messages::srv::CiotpfAppNodeManualTopicConfig_Request>::value &&
    has_bounded_size<ciotpf_messages::srv::CiotpfAppNodeManualTopicConfig_Response>::value
  >
{
};

}  // namespace rosidl_generator_traits

#endif  // CIOTPF_MESSAGES__SRV__CIOTPF_APP_NODE_MANUAL_TOPIC_CONFIG__TRAITS_HPP_
