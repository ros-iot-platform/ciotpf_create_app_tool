// generated from rosidl_generator_c/resource/idl.h.em
// with input from ciotpf_messages:srv/CiotpfAppNodeManualTopicConfig.idl
// generated code does not contain a copyright notice

#ifndef CIOTPF_MESSAGES__SRV__CIOTPF_APP_NODE_MANUAL_TOPIC_CONFIG_H_
#define CIOTPF_MESSAGES__SRV__CIOTPF_APP_NODE_MANUAL_TOPIC_CONFIG_H_

#include "ciotpf_messages/srv/ciotpf_app_node_manual_topic_config__struct.h"
#include "ciotpf_messages/srv/ciotpf_app_node_manual_topic_config__functions.h"
#include "ciotpf_messages/srv/ciotpf_app_node_manual_topic_config__type_support.h"

#endif  // CIOTPF_MESSAGES__SRV__CIOTPF_APP_NODE_MANUAL_TOPIC_CONFIG_H_
