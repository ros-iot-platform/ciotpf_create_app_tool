// generated from rosidl_generator_c/resource/idl__struct.h.em
// with input from ciotpf_messages:srv/CiotpfAppNodeManualTopicConfig.idl
// generated code does not contain a copyright notice

#ifndef CIOTPF_MESSAGES__SRV__CIOTPF_APP_NODE_MANUAL_TOPIC_CONFIG__STRUCT_H_
#define CIOTPF_MESSAGES__SRV__CIOTPF_APP_NODE_MANUAL_TOPIC_CONFIG__STRUCT_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>


// Constants defined in the message

// Include directives for member types
// Member 'topic_name'
// Member 'name'
#include "rosidl_generator_c/string.h"

// Struct defined in srv/CiotpfAppNodeManualTopicConfig in the package ciotpf_messages.
typedef struct ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request
{
  bool is_subscription;
  bool is_add;
  rosidl_generator_c__String topic_name;
  rosidl_generator_c__String name;
} ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request;

// Struct for a sequence of ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request.
typedef struct ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request__Sequence
{
  ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request * data;
  /// The number of valid items in data
  size_t size;
  /// The number of allocated items in data
  size_t capacity;
} ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request__Sequence;


// Constants defined in the message

// Include directives for member types
// Member 'message'
// already included above
// #include "rosidl_generator_c/string.h"

// Struct defined in srv/CiotpfAppNodeManualTopicConfig in the package ciotpf_messages.
typedef struct ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response
{
  bool is_success;
  rosidl_generator_c__String message;
} ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response;

// Struct for a sequence of ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response.
typedef struct ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response__Sequence
{
  ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response * data;
  /// The number of valid items in data
  size_t size;
  /// The number of allocated items in data
  size_t capacity;
} ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response__Sequence;

#ifdef __cplusplus
}
#endif

#endif  // CIOTPF_MESSAGES__SRV__CIOTPF_APP_NODE_MANUAL_TOPIC_CONFIG__STRUCT_H_
