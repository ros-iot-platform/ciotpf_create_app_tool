// generated from rosidl_typesupport_introspection_cpp/resource/idl__type_support.cpp.em
// with input from ciotpf_messages:srv/CiotpfAppNodeInfo.idl
// generated code does not contain a copyright notice

#include "array"
#include "cstddef"
#include "string"
#include "vector"
#include "rosidl_generator_c/message_type_support_struct.h"
#include "rosidl_typesupport_cpp/message_type_support.hpp"
#include "rosidl_typesupport_interface/macros.h"
#include "ciotpf_messages/srv/ciotpf_app_node_info__struct.hpp"
#include "rosidl_typesupport_introspection_cpp/field_types.hpp"
#include "rosidl_typesupport_introspection_cpp/identifier.hpp"
#include "rosidl_typesupport_introspection_cpp/message_introspection.hpp"
#include "rosidl_typesupport_introspection_cpp/message_type_support_decl.hpp"
#include "rosidl_typesupport_introspection_cpp/visibility_control.h"

namespace ciotpf_messages
{

namespace srv
{

namespace rosidl_typesupport_introspection_cpp
{

static const ::rosidl_typesupport_introspection_cpp::MessageMember CiotpfAppNodeInfo_Request_message_member_array[1] = {
  {
    "structure_needs_at_least_one_member",  // name
    ::rosidl_typesupport_introspection_cpp::ROS_TYPE_UINT8,  // type
    0,  // upper bound of string
    NULL,  // members of sub message
    false,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(ciotpf_messages::srv::CiotpfAppNodeInfo_Request, structure_needs_at_least_one_member),  // bytes offset in struct
    nullptr,  // default value
    nullptr,  // size() function pointer
    nullptr,  // get_const(index) function pointer
    nullptr,  // get(index) function pointer
    NULL  // resize(index) function pointer
  }
};

static const ::rosidl_typesupport_introspection_cpp::MessageMembers CiotpfAppNodeInfo_Request_message_members = {
  "ciotpf_messages::srv",  // message namespace
  "CiotpfAppNodeInfo_Request",  // message name
  1,  // number of fields
  sizeof(ciotpf_messages::srv::CiotpfAppNodeInfo_Request),
  CiotpfAppNodeInfo_Request_message_member_array  // message members
};

static const rosidl_message_type_support_t CiotpfAppNodeInfo_Request_message_type_support_handle = {
  ::rosidl_typesupport_introspection_cpp::typesupport_identifier,
  &CiotpfAppNodeInfo_Request_message_members,
  get_message_typesupport_handle_function,
};

}  // namespace rosidl_typesupport_introspection_cpp

}  // namespace srv

}  // namespace ciotpf_messages


namespace rosidl_typesupport_introspection_cpp
{

template<>
ROSIDL_TYPESUPPORT_INTROSPECTION_CPP_PUBLIC
const rosidl_message_type_support_t *
get_message_type_support_handle<ciotpf_messages::srv::CiotpfAppNodeInfo_Request>()
{
  return &::ciotpf_messages::srv::rosidl_typesupport_introspection_cpp::CiotpfAppNodeInfo_Request_message_type_support_handle;
}

}  // namespace rosidl_typesupport_introspection_cpp

#ifdef __cplusplus
extern "C"
{
#endif

ROSIDL_TYPESUPPORT_INTROSPECTION_CPP_PUBLIC
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_cpp, ciotpf_messages, srv, CiotpfAppNodeInfo_Request)() {
  return &::ciotpf_messages::srv::rosidl_typesupport_introspection_cpp::CiotpfAppNodeInfo_Request_message_type_support_handle;
}

#ifdef __cplusplus
}
#endif

// already included above
// #include "array"
// already included above
// #include "cstddef"
// already included above
// #include "string"
// already included above
// #include "vector"
// already included above
// #include "rosidl_generator_c/message_type_support_struct.h"
// already included above
// #include "rosidl_typesupport_cpp/message_type_support.hpp"
// already included above
// #include "rosidl_typesupport_interface/macros.h"
// already included above
// #include "ciotpf_messages/srv/ciotpf_app_node_info__struct.hpp"
// already included above
// #include "rosidl_typesupport_introspection_cpp/field_types.hpp"
// already included above
// #include "rosidl_typesupport_introspection_cpp/identifier.hpp"
// already included above
// #include "rosidl_typesupport_introspection_cpp/message_introspection.hpp"
// already included above
// #include "rosidl_typesupport_introspection_cpp/message_type_support_decl.hpp"
// already included above
// #include "rosidl_typesupport_introspection_cpp/visibility_control.h"

namespace ciotpf_messages
{

namespace srv
{

namespace rosidl_typesupport_introspection_cpp
{

size_t size_function__CiotpfAppNodeInfo_Response__subscriptions(const void * untyped_member)
{
  const auto * member = reinterpret_cast<const std::vector<ciotpf_messages::msg::CiotpfManualTopic> *>(untyped_member);
  return member->size();
}

const void * get_const_function__CiotpfAppNodeInfo_Response__subscriptions(const void * untyped_member, size_t index)
{
  const auto & member =
    *reinterpret_cast<const std::vector<ciotpf_messages::msg::CiotpfManualTopic> *>(untyped_member);
  return &member[index];
}

void * get_function__CiotpfAppNodeInfo_Response__subscriptions(void * untyped_member, size_t index)
{
  auto & member =
    *reinterpret_cast<std::vector<ciotpf_messages::msg::CiotpfManualTopic> *>(untyped_member);
  return &member[index];
}

void resize_function__CiotpfAppNodeInfo_Response__subscriptions(void * untyped_member, size_t size)
{
  auto * member =
    reinterpret_cast<std::vector<ciotpf_messages::msg::CiotpfManualTopic> *>(untyped_member);
  member->resize(size);
}

size_t size_function__CiotpfAppNodeInfo_Response__publishers(const void * untyped_member)
{
  const auto * member = reinterpret_cast<const std::vector<ciotpf_messages::msg::CiotpfManualTopic> *>(untyped_member);
  return member->size();
}

const void * get_const_function__CiotpfAppNodeInfo_Response__publishers(const void * untyped_member, size_t index)
{
  const auto & member =
    *reinterpret_cast<const std::vector<ciotpf_messages::msg::CiotpfManualTopic> *>(untyped_member);
  return &member[index];
}

void * get_function__CiotpfAppNodeInfo_Response__publishers(void * untyped_member, size_t index)
{
  auto & member =
    *reinterpret_cast<std::vector<ciotpf_messages::msg::CiotpfManualTopic> *>(untyped_member);
  return &member[index];
}

void resize_function__CiotpfAppNodeInfo_Response__publishers(void * untyped_member, size_t size)
{
  auto * member =
    reinterpret_cast<std::vector<ciotpf_messages::msg::CiotpfManualTopic> *>(untyped_member);
  member->resize(size);
}

static const ::rosidl_typesupport_introspection_cpp::MessageMember CiotpfAppNodeInfo_Response_message_member_array[2] = {
  {
    "subscriptions",  // name
    ::rosidl_typesupport_introspection_cpp::ROS_TYPE_MESSAGE,  // type
    0,  // upper bound of string
    ::rosidl_typesupport_introspection_cpp::get_message_type_support_handle<ciotpf_messages::msg::CiotpfManualTopic>(),  // members of sub message
    true,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(ciotpf_messages::srv::CiotpfAppNodeInfo_Response, subscriptions),  // bytes offset in struct
    nullptr,  // default value
    size_function__CiotpfAppNodeInfo_Response__subscriptions,  // size() function pointer
    get_const_function__CiotpfAppNodeInfo_Response__subscriptions,  // get_const(index) function pointer
    get_function__CiotpfAppNodeInfo_Response__subscriptions,  // get(index) function pointer
    resize_function__CiotpfAppNodeInfo_Response__subscriptions  // resize(index) function pointer
  },
  {
    "publishers",  // name
    ::rosidl_typesupport_introspection_cpp::ROS_TYPE_MESSAGE,  // type
    0,  // upper bound of string
    ::rosidl_typesupport_introspection_cpp::get_message_type_support_handle<ciotpf_messages::msg::CiotpfManualTopic>(),  // members of sub message
    true,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(ciotpf_messages::srv::CiotpfAppNodeInfo_Response, publishers),  // bytes offset in struct
    nullptr,  // default value
    size_function__CiotpfAppNodeInfo_Response__publishers,  // size() function pointer
    get_const_function__CiotpfAppNodeInfo_Response__publishers,  // get_const(index) function pointer
    get_function__CiotpfAppNodeInfo_Response__publishers,  // get(index) function pointer
    resize_function__CiotpfAppNodeInfo_Response__publishers  // resize(index) function pointer
  }
};

static const ::rosidl_typesupport_introspection_cpp::MessageMembers CiotpfAppNodeInfo_Response_message_members = {
  "ciotpf_messages::srv",  // message namespace
  "CiotpfAppNodeInfo_Response",  // message name
  2,  // number of fields
  sizeof(ciotpf_messages::srv::CiotpfAppNodeInfo_Response),
  CiotpfAppNodeInfo_Response_message_member_array  // message members
};

static const rosidl_message_type_support_t CiotpfAppNodeInfo_Response_message_type_support_handle = {
  ::rosidl_typesupport_introspection_cpp::typesupport_identifier,
  &CiotpfAppNodeInfo_Response_message_members,
  get_message_typesupport_handle_function,
};

}  // namespace rosidl_typesupport_introspection_cpp

}  // namespace srv

}  // namespace ciotpf_messages


namespace rosidl_typesupport_introspection_cpp
{

template<>
ROSIDL_TYPESUPPORT_INTROSPECTION_CPP_PUBLIC
const rosidl_message_type_support_t *
get_message_type_support_handle<ciotpf_messages::srv::CiotpfAppNodeInfo_Response>()
{
  return &::ciotpf_messages::srv::rosidl_typesupport_introspection_cpp::CiotpfAppNodeInfo_Response_message_type_support_handle;
}

}  // namespace rosidl_typesupport_introspection_cpp

#ifdef __cplusplus
extern "C"
{
#endif

ROSIDL_TYPESUPPORT_INTROSPECTION_CPP_PUBLIC
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_cpp, ciotpf_messages, srv, CiotpfAppNodeInfo_Response)() {
  return &::ciotpf_messages::srv::rosidl_typesupport_introspection_cpp::CiotpfAppNodeInfo_Response_message_type_support_handle;
}

#ifdef __cplusplus
}
#endif

#include "rosidl_generator_c/service_type_support_struct.h"
// already included above
// #include "rosidl_typesupport_cpp/message_type_support.hpp"
#include "rosidl_typesupport_cpp/service_type_support.hpp"
// already included above
// #include "rosidl_typesupport_interface/macros.h"
// already included above
// #include "rosidl_typesupport_introspection_cpp/visibility_control.h"
// already included above
// #include "ciotpf_messages/srv/ciotpf_app_node_info__struct.hpp"
// already included above
// #include "rosidl_typesupport_introspection_cpp/identifier.hpp"
// already included above
// #include "rosidl_typesupport_introspection_cpp/message_type_support_decl.hpp"
#include "rosidl_typesupport_introspection_cpp/service_introspection.hpp"
#include "rosidl_typesupport_introspection_cpp/service_type_support_decl.hpp"

namespace ciotpf_messages
{

namespace srv
{

namespace rosidl_typesupport_introspection_cpp
{

// this is intentionally not const to allow initialization later to prevent an initialization race
static ::rosidl_typesupport_introspection_cpp::ServiceMembers CiotpfAppNodeInfo_service_members = {
  "ciotpf_messages::srv",  // service namespace
  "CiotpfAppNodeInfo",  // service name
  // these two fields are initialized below on the first access
  // see get_service_type_support_handle<ciotpf_messages::srv::CiotpfAppNodeInfo>()
  nullptr,  // request message
  nullptr  // response message
};

static const rosidl_service_type_support_t CiotpfAppNodeInfo_service_type_support_handle = {
  ::rosidl_typesupport_introspection_cpp::typesupport_identifier,
  &CiotpfAppNodeInfo_service_members,
  get_service_typesupport_handle_function,
};

}  // namespace rosidl_typesupport_introspection_cpp

}  // namespace srv

}  // namespace ciotpf_messages


namespace rosidl_typesupport_introspection_cpp
{

template<>
ROSIDL_TYPESUPPORT_INTROSPECTION_CPP_PUBLIC
const rosidl_service_type_support_t *
get_service_type_support_handle<ciotpf_messages::srv::CiotpfAppNodeInfo>()
{
  // get a handle to the value to be returned
  auto service_type_support =
    &::ciotpf_messages::srv::rosidl_typesupport_introspection_cpp::CiotpfAppNodeInfo_service_type_support_handle;
  // get a non-const and properly typed version of the data void *
  auto service_members = const_cast<::rosidl_typesupport_introspection_cpp::ServiceMembers *>(
    static_cast<const ::rosidl_typesupport_introspection_cpp::ServiceMembers *>(
      service_type_support->data));
  // make sure that both the request_members_ and the response_members_ are initialized
  // if they are not, initialize them
  if (
    service_members->request_members_ == nullptr ||
    service_members->response_members_ == nullptr)
  {
    // initialize the request_members_ with the static function from the external library
    service_members->request_members_ = static_cast<
      const ::rosidl_typesupport_introspection_cpp::MessageMembers *
      >(
      ::rosidl_typesupport_introspection_cpp::get_message_type_support_handle<
        ::ciotpf_messages::srv::CiotpfAppNodeInfo_Request
      >()->data
      );
    // initialize the response_members_ with the static function from the external library
    service_members->response_members_ = static_cast<
      const ::rosidl_typesupport_introspection_cpp::MessageMembers *
      >(
      ::rosidl_typesupport_introspection_cpp::get_message_type_support_handle<
        ::ciotpf_messages::srv::CiotpfAppNodeInfo_Response
      >()->data
      );
  }
  // finally return the properly initialized service_type_support handle
  return service_type_support;
}

}  // namespace rosidl_typesupport_introspection_cpp

#ifdef __cplusplus
extern "C"
{
#endif

ROSIDL_TYPESUPPORT_INTROSPECTION_CPP_PUBLIC
const rosidl_service_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__SERVICE_SYMBOL_NAME(rosidl_typesupport_introspection_cpp, ciotpf_messages, srv, CiotpfAppNodeInfo)() {
  return ::rosidl_typesupport_introspection_cpp::get_service_type_support_handle<ciotpf_messages::srv::CiotpfAppNodeInfo>();
}

#ifdef __cplusplus
}
#endif
