// generated from rosidl_typesupport_fastrtps_c/resource/idl__rosidl_typesupport_fastrtps_c.h.em
// with input from ciotpf_messages:srv/CiotpfAppNodeManualTopicConfig.idl
// generated code does not contain a copyright notice
#ifndef CIOTPF_MESSAGES__SRV__CIOTPF_APP_NODE_MANUAL_TOPIC_CONFIG__ROSIDL_TYPESUPPORT_FASTRTPS_C_H_
#define CIOTPF_MESSAGES__SRV__CIOTPF_APP_NODE_MANUAL_TOPIC_CONFIG__ROSIDL_TYPESUPPORT_FASTRTPS_C_H_


#include <stddef.h>
#include "rosidl_generator_c/message_type_support_struct.h"
#include "rosidl_typesupport_interface/macros.h"
#include "ciotpf_messages/msg/rosidl_typesupport_fastrtps_c__visibility_control.h"

#ifdef __cplusplus
extern "C"
{
#endif

ROSIDL_TYPESUPPORT_FASTRTPS_C_PUBLIC_ciotpf_messages
size_t get_serialized_size_ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request(
  const void * untyped_ros_message,
  size_t current_alignment);

ROSIDL_TYPESUPPORT_FASTRTPS_C_PUBLIC_ciotpf_messages
size_t max_serialized_size_ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request(
  bool & full_bounded,
  size_t current_alignment);

ROSIDL_TYPESUPPORT_FASTRTPS_C_PUBLIC_ciotpf_messages
const rosidl_message_type_support_t *
  ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_fastrtps_c, ciotpf_messages, srv, CiotpfAppNodeManualTopicConfig_Request)();

#ifdef __cplusplus
}
#endif

// already included above
// #include <stddef.h>
// already included above
// #include "rosidl_generator_c/message_type_support_struct.h"
// already included above
// #include "rosidl_typesupport_interface/macros.h"
// already included above
// #include "ciotpf_messages/msg/rosidl_typesupport_fastrtps_c__visibility_control.h"

#ifdef __cplusplus
extern "C"
{
#endif

ROSIDL_TYPESUPPORT_FASTRTPS_C_PUBLIC_ciotpf_messages
size_t get_serialized_size_ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response(
  const void * untyped_ros_message,
  size_t current_alignment);

ROSIDL_TYPESUPPORT_FASTRTPS_C_PUBLIC_ciotpf_messages
size_t max_serialized_size_ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response(
  bool & full_bounded,
  size_t current_alignment);

ROSIDL_TYPESUPPORT_FASTRTPS_C_PUBLIC_ciotpf_messages
const rosidl_message_type_support_t *
  ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_fastrtps_c, ciotpf_messages, srv, CiotpfAppNodeManualTopicConfig_Response)();

#ifdef __cplusplus
}
#endif

#include "rosidl_generator_c/service_type_support_struct.h"
// already included above
// #include "rosidl_typesupport_interface/macros.h"
// already included above
// #include "ciotpf_messages/msg/rosidl_typesupport_fastrtps_c__visibility_control.h"

#ifdef __cplusplus
extern "C"
{
#endif

ROSIDL_TYPESUPPORT_FASTRTPS_C_PUBLIC_ciotpf_messages
const rosidl_service_type_support_t *
  ROSIDL_TYPESUPPORT_INTERFACE__SERVICE_SYMBOL_NAME(rosidl_typesupport_fastrtps_c, ciotpf_messages, srv, CiotpfAppNodeManualTopicConfig)();

#ifdef __cplusplus
}
#endif

#endif  // CIOTPF_MESSAGES__SRV__CIOTPF_APP_NODE_MANUAL_TOPIC_CONFIG__ROSIDL_TYPESUPPORT_FASTRTPS_C_H_
