// generated from rosidl_generator_c/resource/idl__struct.h.em
// with input from ciotpf_messages:srv/CiotpfAppNodeInfo.idl
// generated code does not contain a copyright notice

#ifndef CIOTPF_MESSAGES__SRV__CIOTPF_APP_NODE_INFO__STRUCT_H_
#define CIOTPF_MESSAGES__SRV__CIOTPF_APP_NODE_INFO__STRUCT_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>


// Constants defined in the message

// Struct defined in srv/CiotpfAppNodeInfo in the package ciotpf_messages.
typedef struct ciotpf_messages__srv__CiotpfAppNodeInfo_Request
{
  uint8_t structure_needs_at_least_one_member;
} ciotpf_messages__srv__CiotpfAppNodeInfo_Request;

// Struct for a sequence of ciotpf_messages__srv__CiotpfAppNodeInfo_Request.
typedef struct ciotpf_messages__srv__CiotpfAppNodeInfo_Request__Sequence
{
  ciotpf_messages__srv__CiotpfAppNodeInfo_Request * data;
  /// The number of valid items in data
  size_t size;
  /// The number of allocated items in data
  size_t capacity;
} ciotpf_messages__srv__CiotpfAppNodeInfo_Request__Sequence;


// Constants defined in the message

// Include directives for member types
// Member 'subscriptions'
// Member 'publishers'
#include "ciotpf_messages/msg/ciotpf_manual_topic__struct.h"

// Struct defined in srv/CiotpfAppNodeInfo in the package ciotpf_messages.
typedef struct ciotpf_messages__srv__CiotpfAppNodeInfo_Response
{
  ciotpf_messages__msg__CiotpfManualTopic__Sequence subscriptions;
  ciotpf_messages__msg__CiotpfManualTopic__Sequence publishers;
} ciotpf_messages__srv__CiotpfAppNodeInfo_Response;

// Struct for a sequence of ciotpf_messages__srv__CiotpfAppNodeInfo_Response.
typedef struct ciotpf_messages__srv__CiotpfAppNodeInfo_Response__Sequence
{
  ciotpf_messages__srv__CiotpfAppNodeInfo_Response * data;
  /// The number of valid items in data
  size_t size;
  /// The number of allocated items in data
  size_t capacity;
} ciotpf_messages__srv__CiotpfAppNodeInfo_Response__Sequence;

#ifdef __cplusplus
}
#endif

#endif  // CIOTPF_MESSAGES__SRV__CIOTPF_APP_NODE_INFO__STRUCT_H_
