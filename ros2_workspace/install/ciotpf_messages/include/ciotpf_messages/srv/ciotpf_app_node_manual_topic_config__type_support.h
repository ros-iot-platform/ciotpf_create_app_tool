// generated from rosidl_generator_c/resource/idl__type_support.h.em
// with input from ciotpf_messages:srv/CiotpfAppNodeManualTopicConfig.idl
// generated code does not contain a copyright notice

#ifndef CIOTPF_MESSAGES__SRV__CIOTPF_APP_NODE_MANUAL_TOPIC_CONFIG__TYPE_SUPPORT_H_
#define CIOTPF_MESSAGES__SRV__CIOTPF_APP_NODE_MANUAL_TOPIC_CONFIG__TYPE_SUPPORT_H_

#include "rosidl_typesupport_interface/macros.h"

#include "ciotpf_messages/msg/rosidl_generator_c__visibility_control.h"

#ifdef __cplusplus
extern "C"
{
#endif

#include "rosidl_generator_c/message_type_support_struct.h"

// Forward declare the get type support functions for this type.
ROSIDL_GENERATOR_C_PUBLIC_ciotpf_messages
const rosidl_message_type_support_t *
  ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(
  rosidl_typesupport_c,
  ciotpf_messages,
  srv,
  CiotpfAppNodeManualTopicConfig_Request
)();

// already included above
// #include "rosidl_generator_c/message_type_support_struct.h"

// Forward declare the get type support functions for this type.
ROSIDL_GENERATOR_C_PUBLIC_ciotpf_messages
const rosidl_message_type_support_t *
  ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(
  rosidl_typesupport_c,
  ciotpf_messages,
  srv,
  CiotpfAppNodeManualTopicConfig_Response
)();

#include "rosidl_generator_c/service_type_support_struct.h"

// Forward declare the get type support functions for this type.
ROSIDL_GENERATOR_C_PUBLIC_ciotpf_messages
const rosidl_service_type_support_t *
  ROSIDL_TYPESUPPORT_INTERFACE__SERVICE_SYMBOL_NAME(
  rosidl_typesupport_c,
  ciotpf_messages,
  srv,
  CiotpfAppNodeManualTopicConfig
)();

#ifdef __cplusplus
}
#endif

#endif  // CIOTPF_MESSAGES__SRV__CIOTPF_APP_NODE_MANUAL_TOPIC_CONFIG__TYPE_SUPPORT_H_
