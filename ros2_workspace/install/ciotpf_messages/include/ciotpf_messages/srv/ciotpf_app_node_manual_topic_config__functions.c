// generated from rosidl_generator_c/resource/idl__functions.c.em
// with input from ciotpf_messages:srv/CiotpfAppNodeManualTopicConfig.idl
// generated code does not contain a copyright notice
#include "ciotpf_messages/srv/ciotpf_app_node_manual_topic_config__functions.h"

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

// Include directives for member types
// Member `topic_name`
// Member `name`
#include "rosidl_generator_c/string_functions.h"

bool
ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request__init(ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request * msg)
{
  if (!msg) {
    return false;
  }
  // is_subscription
  // is_add
  // topic_name
  if (!rosidl_generator_c__String__init(&msg->topic_name)) {
    ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request__fini(msg);
    return false;
  }
  // name
  if (!rosidl_generator_c__String__init(&msg->name)) {
    ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request__fini(msg);
    return false;
  }
  return true;
}

void
ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request__fini(ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request * msg)
{
  if (!msg) {
    return;
  }
  // is_subscription
  // is_add
  // topic_name
  rosidl_generator_c__String__fini(&msg->topic_name);
  // name
  rosidl_generator_c__String__fini(&msg->name);
}

ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request *
ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request__create()
{
  ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request * msg = (ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request *)malloc(sizeof(ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request));
  if (!msg) {
    return NULL;
  }
  memset(msg, 0, sizeof(ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request));
  bool success = ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request__init(msg);
  if (!success) {
    free(msg);
    return NULL;
  }
  return msg;
}

void
ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request__destroy(ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request * msg)
{
  if (msg) {
    ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request__fini(msg);
  }
  free(msg);
}


bool
ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request__Sequence__init(ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request__Sequence * array, size_t size)
{
  if (!array) {
    return false;
  }
  ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request * data = NULL;
  if (size) {
    data = (ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request *)calloc(size, sizeof(ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request));
    if (!data) {
      return false;
    }
    // initialize all array elements
    size_t i;
    for (i = 0; i < size; ++i) {
      bool success = ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request__init(&data[i]);
      if (!success) {
        break;
      }
    }
    if (i < size) {
      // if initialization failed finalize the already initialized array elements
      for (; i > 0; --i) {
        ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request__fini(&data[i - 1]);
      }
      free(data);
      return false;
    }
  }
  array->data = data;
  array->size = size;
  array->capacity = size;
  return true;
}

void
ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request__Sequence__fini(ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request__Sequence * array)
{
  if (!array) {
    return;
  }
  if (array->data) {
    // ensure that data and capacity values are consistent
    assert(array->capacity > 0);
    // finalize all array elements
    for (size_t i = 0; i < array->capacity; ++i) {
      ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request__fini(&array->data[i]);
    }
    free(array->data);
    array->data = NULL;
    array->size = 0;
    array->capacity = 0;
  } else {
    // ensure that data, size, and capacity values are consistent
    assert(0 == array->size);
    assert(0 == array->capacity);
  }
}

ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request__Sequence *
ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request__Sequence__create(size_t size)
{
  ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request__Sequence * array = (ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request__Sequence *)malloc(sizeof(ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request__Sequence));
  if (!array) {
    return NULL;
  }
  bool success = ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request__Sequence__init(array, size);
  if (!success) {
    free(array);
    return NULL;
  }
  return array;
}

void
ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request__Sequence__destroy(ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request__Sequence * array)
{
  if (array) {
    ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request__Sequence__fini(array);
  }
  free(array);
}


// Include directives for member types
// Member `message`
// already included above
// #include "rosidl_generator_c/string_functions.h"

bool
ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response__init(ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response * msg)
{
  if (!msg) {
    return false;
  }
  // is_success
  // message
  if (!rosidl_generator_c__String__init(&msg->message)) {
    ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response__fini(msg);
    return false;
  }
  return true;
}

void
ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response__fini(ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response * msg)
{
  if (!msg) {
    return;
  }
  // is_success
  // message
  rosidl_generator_c__String__fini(&msg->message);
}

ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response *
ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response__create()
{
  ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response * msg = (ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response *)malloc(sizeof(ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response));
  if (!msg) {
    return NULL;
  }
  memset(msg, 0, sizeof(ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response));
  bool success = ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response__init(msg);
  if (!success) {
    free(msg);
    return NULL;
  }
  return msg;
}

void
ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response__destroy(ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response * msg)
{
  if (msg) {
    ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response__fini(msg);
  }
  free(msg);
}


bool
ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response__Sequence__init(ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response__Sequence * array, size_t size)
{
  if (!array) {
    return false;
  }
  ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response * data = NULL;
  if (size) {
    data = (ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response *)calloc(size, sizeof(ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response));
    if (!data) {
      return false;
    }
    // initialize all array elements
    size_t i;
    for (i = 0; i < size; ++i) {
      bool success = ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response__init(&data[i]);
      if (!success) {
        break;
      }
    }
    if (i < size) {
      // if initialization failed finalize the already initialized array elements
      for (; i > 0; --i) {
        ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response__fini(&data[i - 1]);
      }
      free(data);
      return false;
    }
  }
  array->data = data;
  array->size = size;
  array->capacity = size;
  return true;
}

void
ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response__Sequence__fini(ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response__Sequence * array)
{
  if (!array) {
    return;
  }
  if (array->data) {
    // ensure that data and capacity values are consistent
    assert(array->capacity > 0);
    // finalize all array elements
    for (size_t i = 0; i < array->capacity; ++i) {
      ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response__fini(&array->data[i]);
    }
    free(array->data);
    array->data = NULL;
    array->size = 0;
    array->capacity = 0;
  } else {
    // ensure that data, size, and capacity values are consistent
    assert(0 == array->size);
    assert(0 == array->capacity);
  }
}

ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response__Sequence *
ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response__Sequence__create(size_t size)
{
  ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response__Sequence * array = (ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response__Sequence *)malloc(sizeof(ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response__Sequence));
  if (!array) {
    return NULL;
  }
  bool success = ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response__Sequence__init(array, size);
  if (!success) {
    free(array);
    return NULL;
  }
  return array;
}

void
ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response__Sequence__destroy(ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response__Sequence * array)
{
  if (array) {
    ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response__Sequence__fini(array);
  }
  free(array);
}
