// generated from rosidl_typesupport_introspection_c/resource/idl__type_support.c.em
// with input from ciotpf_messages:srv/CiotpfAppNodeManualTopicConfig.idl
// generated code does not contain a copyright notice

#include <stddef.h>
#include "ciotpf_messages/srv/ciotpf_app_node_manual_topic_config__rosidl_typesupport_introspection_c.h"
#include "ciotpf_messages/msg/rosidl_typesupport_introspection_c__visibility_control.h"
#include "rosidl_typesupport_introspection_c/field_types.h"
#include "rosidl_typesupport_introspection_c/identifier.h"
#include "rosidl_typesupport_introspection_c/message_introspection.h"
#include "ciotpf_messages/srv/ciotpf_app_node_manual_topic_config__struct.h"


// Include directives for member types
// Member `topic_name`
// Member `name`
#include "rosidl_generator_c/string_functions.h"

#ifdef __cplusplus
extern "C"
{
#endif

static rosidl_typesupport_introspection_c__MessageMember CiotpfAppNodeManualTopicConfig_Request__rosidl_typesupport_introspection_c__CiotpfAppNodeManualTopicConfig_Request_message_member_array[4] = {
  {
    "is_subscription",  // name
    rosidl_typesupport_introspection_c__ROS_TYPE_BOOLEAN,  // type
    0,  // upper bound of string
    NULL,  // members of sub message
    false,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request, is_subscription),  // bytes offset in struct
    NULL,  // default value
    NULL,  // size() function pointer
    NULL,  // get_const(index) function pointer
    NULL,  // get(index) function pointer
    NULL  // resize(index) function pointer
  },
  {
    "is_add",  // name
    rosidl_typesupport_introspection_c__ROS_TYPE_BOOLEAN,  // type
    0,  // upper bound of string
    NULL,  // members of sub message
    false,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request, is_add),  // bytes offset in struct
    NULL,  // default value
    NULL,  // size() function pointer
    NULL,  // get_const(index) function pointer
    NULL,  // get(index) function pointer
    NULL  // resize(index) function pointer
  },
  {
    "topic_name",  // name
    rosidl_typesupport_introspection_c__ROS_TYPE_STRING,  // type
    0,  // upper bound of string
    NULL,  // members of sub message
    false,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request, topic_name),  // bytes offset in struct
    NULL,  // default value
    NULL,  // size() function pointer
    NULL,  // get_const(index) function pointer
    NULL,  // get(index) function pointer
    NULL  // resize(index) function pointer
  },
  {
    "name",  // name
    rosidl_typesupport_introspection_c__ROS_TYPE_STRING,  // type
    0,  // upper bound of string
    NULL,  // members of sub message
    false,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request, name),  // bytes offset in struct
    NULL,  // default value
    NULL,  // size() function pointer
    NULL,  // get_const(index) function pointer
    NULL,  // get(index) function pointer
    NULL  // resize(index) function pointer
  }
};

static const rosidl_typesupport_introspection_c__MessageMembers CiotpfAppNodeManualTopicConfig_Request__rosidl_typesupport_introspection_c__CiotpfAppNodeManualTopicConfig_Request_message_members = {
  "ciotpf_messages__srv",  // message namespace
  "CiotpfAppNodeManualTopicConfig_Request",  // message name
  4,  // number of fields
  sizeof(ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request),
  CiotpfAppNodeManualTopicConfig_Request__rosidl_typesupport_introspection_c__CiotpfAppNodeManualTopicConfig_Request_message_member_array  // message members
};

// this is not const since it must be initialized on first access
// since C does not allow non-integral compile-time constants
static rosidl_message_type_support_t CiotpfAppNodeManualTopicConfig_Request__rosidl_typesupport_introspection_c__CiotpfAppNodeManualTopicConfig_Request_message_type_support_handle = {
  0,
  &CiotpfAppNodeManualTopicConfig_Request__rosidl_typesupport_introspection_c__CiotpfAppNodeManualTopicConfig_Request_message_members,
  get_message_typesupport_handle_function,
};

ROSIDL_TYPESUPPORT_INTROSPECTION_C_EXPORT_ciotpf_messages
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, ciotpf_messages, srv, CiotpfAppNodeManualTopicConfig_Request)() {
  if (!CiotpfAppNodeManualTopicConfig_Request__rosidl_typesupport_introspection_c__CiotpfAppNodeManualTopicConfig_Request_message_type_support_handle.typesupport_identifier) {
    CiotpfAppNodeManualTopicConfig_Request__rosidl_typesupport_introspection_c__CiotpfAppNodeManualTopicConfig_Request_message_type_support_handle.typesupport_identifier =
      rosidl_typesupport_introspection_c__identifier;
  }
  return &CiotpfAppNodeManualTopicConfig_Request__rosidl_typesupport_introspection_c__CiotpfAppNodeManualTopicConfig_Request_message_type_support_handle;
}
#ifdef __cplusplus
}
#endif

// already included above
// #include <stddef.h>
// already included above
// #include "ciotpf_messages/srv/ciotpf_app_node_manual_topic_config__rosidl_typesupport_introspection_c.h"
// already included above
// #include "ciotpf_messages/msg/rosidl_typesupport_introspection_c__visibility_control.h"
// already included above
// #include "rosidl_typesupport_introspection_c/field_types.h"
// already included above
// #include "rosidl_typesupport_introspection_c/identifier.h"
// already included above
// #include "rosidl_typesupport_introspection_c/message_introspection.h"
// already included above
// #include "ciotpf_messages/srv/ciotpf_app_node_manual_topic_config__struct.h"


// Include directives for member types
// Member `message`
// already included above
// #include "rosidl_generator_c/string_functions.h"

#ifdef __cplusplus
extern "C"
{
#endif

static rosidl_typesupport_introspection_c__MessageMember CiotpfAppNodeManualTopicConfig_Response__rosidl_typesupport_introspection_c__CiotpfAppNodeManualTopicConfig_Response_message_member_array[2] = {
  {
    "is_success",  // name
    rosidl_typesupport_introspection_c__ROS_TYPE_BOOLEAN,  // type
    0,  // upper bound of string
    NULL,  // members of sub message
    false,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response, is_success),  // bytes offset in struct
    NULL,  // default value
    NULL,  // size() function pointer
    NULL,  // get_const(index) function pointer
    NULL,  // get(index) function pointer
    NULL  // resize(index) function pointer
  },
  {
    "message",  // name
    rosidl_typesupport_introspection_c__ROS_TYPE_STRING,  // type
    0,  // upper bound of string
    NULL,  // members of sub message
    false,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response, message),  // bytes offset in struct
    NULL,  // default value
    NULL,  // size() function pointer
    NULL,  // get_const(index) function pointer
    NULL,  // get(index) function pointer
    NULL  // resize(index) function pointer
  }
};

static const rosidl_typesupport_introspection_c__MessageMembers CiotpfAppNodeManualTopicConfig_Response__rosidl_typesupport_introspection_c__CiotpfAppNodeManualTopicConfig_Response_message_members = {
  "ciotpf_messages__srv",  // message namespace
  "CiotpfAppNodeManualTopicConfig_Response",  // message name
  2,  // number of fields
  sizeof(ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response),
  CiotpfAppNodeManualTopicConfig_Response__rosidl_typesupport_introspection_c__CiotpfAppNodeManualTopicConfig_Response_message_member_array  // message members
};

// this is not const since it must be initialized on first access
// since C does not allow non-integral compile-time constants
static rosidl_message_type_support_t CiotpfAppNodeManualTopicConfig_Response__rosidl_typesupport_introspection_c__CiotpfAppNodeManualTopicConfig_Response_message_type_support_handle = {
  0,
  &CiotpfAppNodeManualTopicConfig_Response__rosidl_typesupport_introspection_c__CiotpfAppNodeManualTopicConfig_Response_message_members,
  get_message_typesupport_handle_function,
};

ROSIDL_TYPESUPPORT_INTROSPECTION_C_EXPORT_ciotpf_messages
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, ciotpf_messages, srv, CiotpfAppNodeManualTopicConfig_Response)() {
  if (!CiotpfAppNodeManualTopicConfig_Response__rosidl_typesupport_introspection_c__CiotpfAppNodeManualTopicConfig_Response_message_type_support_handle.typesupport_identifier) {
    CiotpfAppNodeManualTopicConfig_Response__rosidl_typesupport_introspection_c__CiotpfAppNodeManualTopicConfig_Response_message_type_support_handle.typesupport_identifier =
      rosidl_typesupport_introspection_c__identifier;
  }
  return &CiotpfAppNodeManualTopicConfig_Response__rosidl_typesupport_introspection_c__CiotpfAppNodeManualTopicConfig_Response_message_type_support_handle;
}
#ifdef __cplusplus
}
#endif

#include "rosidl_generator_c/service_type_support_struct.h"
// already included above
// #include "ciotpf_messages/msg/rosidl_typesupport_introspection_c__visibility_control.h"
// already included above
// #include "ciotpf_messages/srv/ciotpf_app_node_manual_topic_config__rosidl_typesupport_introspection_c.h"
// already included above
// #include "rosidl_typesupport_introspection_c/identifier.h"
#include "rosidl_typesupport_introspection_c/service_introspection.h"

// this is intentionally not const to allow initialization later to prevent an initialization race
static rosidl_typesupport_introspection_c__ServiceMembers ciotpf_messages__srv__ciotpf_app_node_manual_topic_config__rosidl_typesupport_introspection_c__CiotpfAppNodeManualTopicConfig_service_members = {
  "ciotpf_messages__srv",  // service namespace
  "CiotpfAppNodeManualTopicConfig",  // service name
  // these two fields are initialized below on the first access
  NULL,  // request message
  // ciotpf_messages__srv__ciotpf_app_node_manual_topic_config__rosidl_typesupport_introspection_c__CiotpfAppNodeManualTopicConfig_Request_message_type_support_handle,
  NULL  // response message
  // ciotpf_messages__srv__ciotpf_app_node_manual_topic_config__rosidl_typesupport_introspection_c__CiotpfAppNodeManualTopicConfig_Response_message_type_support_handle
};

static rosidl_service_type_support_t ciotpf_messages__srv__ciotpf_app_node_manual_topic_config__rosidl_typesupport_introspection_c__CiotpfAppNodeManualTopicConfig_service_type_support_handle = {
  0,
  &ciotpf_messages__srv__ciotpf_app_node_manual_topic_config__rosidl_typesupport_introspection_c__CiotpfAppNodeManualTopicConfig_service_members,
  get_service_typesupport_handle_function,
};

// Forward declaration of request/response type support functions
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, ciotpf_messages, srv, CiotpfAppNodeManualTopicConfig_Request)();

const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, ciotpf_messages, srv, CiotpfAppNodeManualTopicConfig_Response)();

ROSIDL_TYPESUPPORT_INTROSPECTION_C_EXPORT_ciotpf_messages
const rosidl_service_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__SERVICE_SYMBOL_NAME(rosidl_typesupport_introspection_c, ciotpf_messages, srv, CiotpfAppNodeManualTopicConfig)() {
  if (!ciotpf_messages__srv__ciotpf_app_node_manual_topic_config__rosidl_typesupport_introspection_c__CiotpfAppNodeManualTopicConfig_service_type_support_handle.typesupport_identifier) {
    ciotpf_messages__srv__ciotpf_app_node_manual_topic_config__rosidl_typesupport_introspection_c__CiotpfAppNodeManualTopicConfig_service_type_support_handle.typesupport_identifier =
      rosidl_typesupport_introspection_c__identifier;
  }
  rosidl_typesupport_introspection_c__ServiceMembers * service_members =
    (rosidl_typesupport_introspection_c__ServiceMembers *)ciotpf_messages__srv__ciotpf_app_node_manual_topic_config__rosidl_typesupport_introspection_c__CiotpfAppNodeManualTopicConfig_service_type_support_handle.data;

  if (!service_members->request_members_) {
    service_members->request_members_ =
      (const rosidl_typesupport_introspection_c__MessageMembers *)
      ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, ciotpf_messages, srv, CiotpfAppNodeManualTopicConfig_Request)()->data;
  }
  if (!service_members->response_members_) {
    service_members->response_members_ =
      (const rosidl_typesupport_introspection_c__MessageMembers *)
      ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, ciotpf_messages, srv, CiotpfAppNodeManualTopicConfig_Response)()->data;
  }

  return &ciotpf_messages__srv__ciotpf_app_node_manual_topic_config__rosidl_typesupport_introspection_c__CiotpfAppNodeManualTopicConfig_service_type_support_handle;
}
