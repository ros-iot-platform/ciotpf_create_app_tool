// generated from rosidl_generator_cpp/resource/idl__struct.hpp.em
// with input from ciotpf_messages:srv/CiotpfAppNodeManualTopicConfig.idl
// generated code does not contain a copyright notice

#ifndef CIOTPF_MESSAGES__SRV__CIOTPF_APP_NODE_MANUAL_TOPIC_CONFIG__STRUCT_HPP_
#define CIOTPF_MESSAGES__SRV__CIOTPF_APP_NODE_MANUAL_TOPIC_CONFIG__STRUCT_HPP_

#include <rosidl_generator_cpp/bounded_vector.hpp>
#include <rosidl_generator_cpp/message_initialization.hpp>
#include <algorithm>
#include <array>
#include <memory>
#include <string>
#include <vector>

// Protect against ERROR being predefined on Windows, in case somebody defines a
// constant by that name.
#if defined(_WIN32)
  #if defined(ERROR)
    #undef ERROR
  #endif
  #if defined(NO_ERROR)
    #undef NO_ERROR
  #endif
#endif

#ifndef _WIN32
# define DEPRECATED__ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request __attribute__((deprecated))
#else
# define DEPRECATED__ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request __declspec(deprecated)
#endif

namespace ciotpf_messages
{

namespace srv
{

// message struct
template<class ContainerAllocator>
struct CiotpfAppNodeManualTopicConfig_Request_
{
  using Type = CiotpfAppNodeManualTopicConfig_Request_<ContainerAllocator>;

  explicit CiotpfAppNodeManualTopicConfig_Request_(rosidl_generator_cpp::MessageInitialization _init = rosidl_generator_cpp::MessageInitialization::ALL)
  {
    if (rosidl_generator_cpp::MessageInitialization::ALL == _init ||
      rosidl_generator_cpp::MessageInitialization::ZERO == _init)
    {
      this->is_subscription = false;
      this->is_add = false;
      this->topic_name = "";
      this->name = "";
    }
  }

  explicit CiotpfAppNodeManualTopicConfig_Request_(const ContainerAllocator & _alloc, rosidl_generator_cpp::MessageInitialization _init = rosidl_generator_cpp::MessageInitialization::ALL)
  : topic_name(_alloc),
    name(_alloc)
  {
    if (rosidl_generator_cpp::MessageInitialization::ALL == _init ||
      rosidl_generator_cpp::MessageInitialization::ZERO == _init)
    {
      this->is_subscription = false;
      this->is_add = false;
      this->topic_name = "";
      this->name = "";
    }
  }

  // field types and members
  using _is_subscription_type =
    bool;
  _is_subscription_type is_subscription;
  using _is_add_type =
    bool;
  _is_add_type is_add;
  using _topic_name_type =
    std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other>;
  _topic_name_type topic_name;
  using _name_type =
    std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other>;
  _name_type name;

  // setters for named parameter idiom
  Type & set__is_subscription(
    const bool & _arg)
  {
    this->is_subscription = _arg;
    return *this;
  }
  Type & set__is_add(
    const bool & _arg)
  {
    this->is_add = _arg;
    return *this;
  }
  Type & set__topic_name(
    const std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other> & _arg)
  {
    this->topic_name = _arg;
    return *this;
  }
  Type & set__name(
    const std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other> & _arg)
  {
    this->name = _arg;
    return *this;
  }

  // constant declarations

  // pointer types
  using RawPtr =
    ciotpf_messages::srv::CiotpfAppNodeManualTopicConfig_Request_<ContainerAllocator> *;
  using ConstRawPtr =
    const ciotpf_messages::srv::CiotpfAppNodeManualTopicConfig_Request_<ContainerAllocator> *;
  using SharedPtr =
    std::shared_ptr<ciotpf_messages::srv::CiotpfAppNodeManualTopicConfig_Request_<ContainerAllocator>>;
  using ConstSharedPtr =
    std::shared_ptr<ciotpf_messages::srv::CiotpfAppNodeManualTopicConfig_Request_<ContainerAllocator> const>;

  template<typename Deleter = std::default_delete<
      ciotpf_messages::srv::CiotpfAppNodeManualTopicConfig_Request_<ContainerAllocator>>>
  using UniquePtrWithDeleter =
    std::unique_ptr<ciotpf_messages::srv::CiotpfAppNodeManualTopicConfig_Request_<ContainerAllocator>, Deleter>;

  using UniquePtr = UniquePtrWithDeleter<>;

  template<typename Deleter = std::default_delete<
      ciotpf_messages::srv::CiotpfAppNodeManualTopicConfig_Request_<ContainerAllocator>>>
  using ConstUniquePtrWithDeleter =
    std::unique_ptr<ciotpf_messages::srv::CiotpfAppNodeManualTopicConfig_Request_<ContainerAllocator> const, Deleter>;
  using ConstUniquePtr = ConstUniquePtrWithDeleter<>;

  using WeakPtr =
    std::weak_ptr<ciotpf_messages::srv::CiotpfAppNodeManualTopicConfig_Request_<ContainerAllocator>>;
  using ConstWeakPtr =
    std::weak_ptr<ciotpf_messages::srv::CiotpfAppNodeManualTopicConfig_Request_<ContainerAllocator> const>;

  // pointer types similar to ROS 1, use SharedPtr / ConstSharedPtr instead
  // NOTE: Can't use 'using' here because GNU C++ can't parse attributes properly
  typedef DEPRECATED__ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request
    std::shared_ptr<ciotpf_messages::srv::CiotpfAppNodeManualTopicConfig_Request_<ContainerAllocator>>
    Ptr;
  typedef DEPRECATED__ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Request
    std::shared_ptr<ciotpf_messages::srv::CiotpfAppNodeManualTopicConfig_Request_<ContainerAllocator> const>
    ConstPtr;

  // comparison operators
  bool operator==(const CiotpfAppNodeManualTopicConfig_Request_ & other) const
  {
    if (this->is_subscription != other.is_subscription) {
      return false;
    }
    if (this->is_add != other.is_add) {
      return false;
    }
    if (this->topic_name != other.topic_name) {
      return false;
    }
    if (this->name != other.name) {
      return false;
    }
    return true;
  }
  bool operator!=(const CiotpfAppNodeManualTopicConfig_Request_ & other) const
  {
    return !this->operator==(other);
  }
};  // struct CiotpfAppNodeManualTopicConfig_Request_

// alias to use template instance with default allocator
using CiotpfAppNodeManualTopicConfig_Request =
  ciotpf_messages::srv::CiotpfAppNodeManualTopicConfig_Request_<std::allocator<void>>;

// constant definitions

}  // namespace srv

}  // namespace ciotpf_messages

// Protect against ERROR being predefined on Windows, in case somebody defines a
// constant by that name.
#if defined(_WIN32)
  #if defined(ERROR)
    #undef ERROR
  #endif
  #if defined(NO_ERROR)
    #undef NO_ERROR
  #endif
#endif

#ifndef _WIN32
# define DEPRECATED__ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response __attribute__((deprecated))
#else
# define DEPRECATED__ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response __declspec(deprecated)
#endif

namespace ciotpf_messages
{

namespace srv
{

// message struct
template<class ContainerAllocator>
struct CiotpfAppNodeManualTopicConfig_Response_
{
  using Type = CiotpfAppNodeManualTopicConfig_Response_<ContainerAllocator>;

  explicit CiotpfAppNodeManualTopicConfig_Response_(rosidl_generator_cpp::MessageInitialization _init = rosidl_generator_cpp::MessageInitialization::ALL)
  {
    if (rosidl_generator_cpp::MessageInitialization::ALL == _init ||
      rosidl_generator_cpp::MessageInitialization::ZERO == _init)
    {
      this->is_success = false;
      this->message = "";
    }
  }

  explicit CiotpfAppNodeManualTopicConfig_Response_(const ContainerAllocator & _alloc, rosidl_generator_cpp::MessageInitialization _init = rosidl_generator_cpp::MessageInitialization::ALL)
  : message(_alloc)
  {
    if (rosidl_generator_cpp::MessageInitialization::ALL == _init ||
      rosidl_generator_cpp::MessageInitialization::ZERO == _init)
    {
      this->is_success = false;
      this->message = "";
    }
  }

  // field types and members
  using _is_success_type =
    bool;
  _is_success_type is_success;
  using _message_type =
    std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other>;
  _message_type message;

  // setters for named parameter idiom
  Type & set__is_success(
    const bool & _arg)
  {
    this->is_success = _arg;
    return *this;
  }
  Type & set__message(
    const std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other> & _arg)
  {
    this->message = _arg;
    return *this;
  }

  // constant declarations

  // pointer types
  using RawPtr =
    ciotpf_messages::srv::CiotpfAppNodeManualTopicConfig_Response_<ContainerAllocator> *;
  using ConstRawPtr =
    const ciotpf_messages::srv::CiotpfAppNodeManualTopicConfig_Response_<ContainerAllocator> *;
  using SharedPtr =
    std::shared_ptr<ciotpf_messages::srv::CiotpfAppNodeManualTopicConfig_Response_<ContainerAllocator>>;
  using ConstSharedPtr =
    std::shared_ptr<ciotpf_messages::srv::CiotpfAppNodeManualTopicConfig_Response_<ContainerAllocator> const>;

  template<typename Deleter = std::default_delete<
      ciotpf_messages::srv::CiotpfAppNodeManualTopicConfig_Response_<ContainerAllocator>>>
  using UniquePtrWithDeleter =
    std::unique_ptr<ciotpf_messages::srv::CiotpfAppNodeManualTopicConfig_Response_<ContainerAllocator>, Deleter>;

  using UniquePtr = UniquePtrWithDeleter<>;

  template<typename Deleter = std::default_delete<
      ciotpf_messages::srv::CiotpfAppNodeManualTopicConfig_Response_<ContainerAllocator>>>
  using ConstUniquePtrWithDeleter =
    std::unique_ptr<ciotpf_messages::srv::CiotpfAppNodeManualTopicConfig_Response_<ContainerAllocator> const, Deleter>;
  using ConstUniquePtr = ConstUniquePtrWithDeleter<>;

  using WeakPtr =
    std::weak_ptr<ciotpf_messages::srv::CiotpfAppNodeManualTopicConfig_Response_<ContainerAllocator>>;
  using ConstWeakPtr =
    std::weak_ptr<ciotpf_messages::srv::CiotpfAppNodeManualTopicConfig_Response_<ContainerAllocator> const>;

  // pointer types similar to ROS 1, use SharedPtr / ConstSharedPtr instead
  // NOTE: Can't use 'using' here because GNU C++ can't parse attributes properly
  typedef DEPRECATED__ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response
    std::shared_ptr<ciotpf_messages::srv::CiotpfAppNodeManualTopicConfig_Response_<ContainerAllocator>>
    Ptr;
  typedef DEPRECATED__ciotpf_messages__srv__CiotpfAppNodeManualTopicConfig_Response
    std::shared_ptr<ciotpf_messages::srv::CiotpfAppNodeManualTopicConfig_Response_<ContainerAllocator> const>
    ConstPtr;

  // comparison operators
  bool operator==(const CiotpfAppNodeManualTopicConfig_Response_ & other) const
  {
    if (this->is_success != other.is_success) {
      return false;
    }
    if (this->message != other.message) {
      return false;
    }
    return true;
  }
  bool operator!=(const CiotpfAppNodeManualTopicConfig_Response_ & other) const
  {
    return !this->operator==(other);
  }
};  // struct CiotpfAppNodeManualTopicConfig_Response_

// alias to use template instance with default allocator
using CiotpfAppNodeManualTopicConfig_Response =
  ciotpf_messages::srv::CiotpfAppNodeManualTopicConfig_Response_<std::allocator<void>>;

// constant definitions

}  // namespace srv

}  // namespace ciotpf_messages

namespace ciotpf_messages
{

namespace srv
{

struct CiotpfAppNodeManualTopicConfig
{
  using Request = ciotpf_messages::srv::CiotpfAppNodeManualTopicConfig_Request;
  using Response = ciotpf_messages::srv::CiotpfAppNodeManualTopicConfig_Response;
};

}  // namespace srv

}  // namespace ciotpf_messages

#endif  // CIOTPF_MESSAGES__SRV__CIOTPF_APP_NODE_MANUAL_TOPIC_CONFIG__STRUCT_HPP_
