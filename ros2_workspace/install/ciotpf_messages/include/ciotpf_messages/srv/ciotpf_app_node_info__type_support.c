// generated from rosidl_typesupport_introspection_c/resource/idl__type_support.c.em
// with input from ciotpf_messages:srv/CiotpfAppNodeInfo.idl
// generated code does not contain a copyright notice

#include <stddef.h>
#include "ciotpf_messages/srv/ciotpf_app_node_info__rosidl_typesupport_introspection_c.h"
#include "ciotpf_messages/msg/rosidl_typesupport_introspection_c__visibility_control.h"
#include "rosidl_typesupport_introspection_c/field_types.h"
#include "rosidl_typesupport_introspection_c/identifier.h"
#include "rosidl_typesupport_introspection_c/message_introspection.h"
#include "ciotpf_messages/srv/ciotpf_app_node_info__struct.h"


#ifdef __cplusplus
extern "C"
{
#endif

static rosidl_typesupport_introspection_c__MessageMember CiotpfAppNodeInfo_Request__rosidl_typesupport_introspection_c__CiotpfAppNodeInfo_Request_message_member_array[1] = {
  {
    "structure_needs_at_least_one_member",  // name
    rosidl_typesupport_introspection_c__ROS_TYPE_UINT8,  // type
    0,  // upper bound of string
    NULL,  // members of sub message
    false,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(ciotpf_messages__srv__CiotpfAppNodeInfo_Request, structure_needs_at_least_one_member),  // bytes offset in struct
    NULL,  // default value
    NULL,  // size() function pointer
    NULL,  // get_const(index) function pointer
    NULL,  // get(index) function pointer
    NULL  // resize(index) function pointer
  }
};

static const rosidl_typesupport_introspection_c__MessageMembers CiotpfAppNodeInfo_Request__rosidl_typesupport_introspection_c__CiotpfAppNodeInfo_Request_message_members = {
  "ciotpf_messages__srv",  // message namespace
  "CiotpfAppNodeInfo_Request",  // message name
  1,  // number of fields
  sizeof(ciotpf_messages__srv__CiotpfAppNodeInfo_Request),
  CiotpfAppNodeInfo_Request__rosidl_typesupport_introspection_c__CiotpfAppNodeInfo_Request_message_member_array  // message members
};

// this is not const since it must be initialized on first access
// since C does not allow non-integral compile-time constants
static rosidl_message_type_support_t CiotpfAppNodeInfo_Request__rosidl_typesupport_introspection_c__CiotpfAppNodeInfo_Request_message_type_support_handle = {
  0,
  &CiotpfAppNodeInfo_Request__rosidl_typesupport_introspection_c__CiotpfAppNodeInfo_Request_message_members,
  get_message_typesupport_handle_function,
};

ROSIDL_TYPESUPPORT_INTROSPECTION_C_EXPORT_ciotpf_messages
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, ciotpf_messages, srv, CiotpfAppNodeInfo_Request)() {
  if (!CiotpfAppNodeInfo_Request__rosidl_typesupport_introspection_c__CiotpfAppNodeInfo_Request_message_type_support_handle.typesupport_identifier) {
    CiotpfAppNodeInfo_Request__rosidl_typesupport_introspection_c__CiotpfAppNodeInfo_Request_message_type_support_handle.typesupport_identifier =
      rosidl_typesupport_introspection_c__identifier;
  }
  return &CiotpfAppNodeInfo_Request__rosidl_typesupport_introspection_c__CiotpfAppNodeInfo_Request_message_type_support_handle;
}
#ifdef __cplusplus
}
#endif

// already included above
// #include <stddef.h>
// already included above
// #include "ciotpf_messages/srv/ciotpf_app_node_info__rosidl_typesupport_introspection_c.h"
// already included above
// #include "ciotpf_messages/msg/rosidl_typesupport_introspection_c__visibility_control.h"
// already included above
// #include "rosidl_typesupport_introspection_c/field_types.h"
// already included above
// #include "rosidl_typesupport_introspection_c/identifier.h"
// already included above
// #include "rosidl_typesupport_introspection_c/message_introspection.h"
// already included above
// #include "ciotpf_messages/srv/ciotpf_app_node_info__struct.h"


// Include directives for member types
// Member `subscriptions`
// Member `publishers`
#include "ciotpf_messages/msg/ciotpf_manual_topic.h"
// Member `subscriptions`
// Member `publishers`
#include "ciotpf_messages/msg/ciotpf_manual_topic__rosidl_typesupport_introspection_c.h"

#ifdef __cplusplus
extern "C"
{
#endif

size_t CiotpfAppNodeInfo_Response__rosidl_typesupport_introspection_c__size_function__CiotpfManualTopic__subscriptions(
  const void * untyped_member)
{
  const ciotpf_messages__msg__CiotpfManualTopic__Sequence * member =
    (const ciotpf_messages__msg__CiotpfManualTopic__Sequence *)(untyped_member);
  return member->size;
}

const void * CiotpfAppNodeInfo_Response__rosidl_typesupport_introspection_c__get_const_function__CiotpfManualTopic__subscriptions(
  const void * untyped_member, size_t index)
{
  const ciotpf_messages__msg__CiotpfManualTopic__Sequence * member =
    (const ciotpf_messages__msg__CiotpfManualTopic__Sequence *)(untyped_member);
  return &member->data[index];
}

void * CiotpfAppNodeInfo_Response__rosidl_typesupport_introspection_c__get_function__CiotpfManualTopic__subscriptions(
  void * untyped_member, size_t index)
{
  ciotpf_messages__msg__CiotpfManualTopic__Sequence * member =
    (ciotpf_messages__msg__CiotpfManualTopic__Sequence *)(untyped_member);
  return &member->data[index];
}

bool CiotpfAppNodeInfo_Response__rosidl_typesupport_introspection_c__resize_function__CiotpfManualTopic__subscriptions(
  void * untyped_member, size_t size)
{
  ciotpf_messages__msg__CiotpfManualTopic__Sequence * member =
    (ciotpf_messages__msg__CiotpfManualTopic__Sequence *)(untyped_member);
  ciotpf_messages__msg__CiotpfManualTopic__Sequence__fini(member);
  return ciotpf_messages__msg__CiotpfManualTopic__Sequence__init(member, size);
}

size_t CiotpfAppNodeInfo_Response__rosidl_typesupport_introspection_c__size_function__CiotpfManualTopic__publishers(
  const void * untyped_member)
{
  const ciotpf_messages__msg__CiotpfManualTopic__Sequence * member =
    (const ciotpf_messages__msg__CiotpfManualTopic__Sequence *)(untyped_member);
  return member->size;
}

const void * CiotpfAppNodeInfo_Response__rosidl_typesupport_introspection_c__get_const_function__CiotpfManualTopic__publishers(
  const void * untyped_member, size_t index)
{
  const ciotpf_messages__msg__CiotpfManualTopic__Sequence * member =
    (const ciotpf_messages__msg__CiotpfManualTopic__Sequence *)(untyped_member);
  return &member->data[index];
}

void * CiotpfAppNodeInfo_Response__rosidl_typesupport_introspection_c__get_function__CiotpfManualTopic__publishers(
  void * untyped_member, size_t index)
{
  ciotpf_messages__msg__CiotpfManualTopic__Sequence * member =
    (ciotpf_messages__msg__CiotpfManualTopic__Sequence *)(untyped_member);
  return &member->data[index];
}

bool CiotpfAppNodeInfo_Response__rosidl_typesupport_introspection_c__resize_function__CiotpfManualTopic__publishers(
  void * untyped_member, size_t size)
{
  ciotpf_messages__msg__CiotpfManualTopic__Sequence * member =
    (ciotpf_messages__msg__CiotpfManualTopic__Sequence *)(untyped_member);
  ciotpf_messages__msg__CiotpfManualTopic__Sequence__fini(member);
  return ciotpf_messages__msg__CiotpfManualTopic__Sequence__init(member, size);
}

static rosidl_typesupport_introspection_c__MessageMember CiotpfAppNodeInfo_Response__rosidl_typesupport_introspection_c__CiotpfAppNodeInfo_Response_message_member_array[2] = {
  {
    "subscriptions",  // name
    rosidl_typesupport_introspection_c__ROS_TYPE_MESSAGE,  // type
    0,  // upper bound of string
    NULL,  // members of sub message (initialized later)
    true,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(ciotpf_messages__srv__CiotpfAppNodeInfo_Response, subscriptions),  // bytes offset in struct
    NULL,  // default value
    CiotpfAppNodeInfo_Response__rosidl_typesupport_introspection_c__size_function__CiotpfManualTopic__subscriptions,  // size() function pointer
    CiotpfAppNodeInfo_Response__rosidl_typesupport_introspection_c__get_const_function__CiotpfManualTopic__subscriptions,  // get_const(index) function pointer
    CiotpfAppNodeInfo_Response__rosidl_typesupport_introspection_c__get_function__CiotpfManualTopic__subscriptions,  // get(index) function pointer
    CiotpfAppNodeInfo_Response__rosidl_typesupport_introspection_c__resize_function__CiotpfManualTopic__subscriptions  // resize(index) function pointer
  },
  {
    "publishers",  // name
    rosidl_typesupport_introspection_c__ROS_TYPE_MESSAGE,  // type
    0,  // upper bound of string
    NULL,  // members of sub message (initialized later)
    true,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(ciotpf_messages__srv__CiotpfAppNodeInfo_Response, publishers),  // bytes offset in struct
    NULL,  // default value
    CiotpfAppNodeInfo_Response__rosidl_typesupport_introspection_c__size_function__CiotpfManualTopic__publishers,  // size() function pointer
    CiotpfAppNodeInfo_Response__rosidl_typesupport_introspection_c__get_const_function__CiotpfManualTopic__publishers,  // get_const(index) function pointer
    CiotpfAppNodeInfo_Response__rosidl_typesupport_introspection_c__get_function__CiotpfManualTopic__publishers,  // get(index) function pointer
    CiotpfAppNodeInfo_Response__rosidl_typesupport_introspection_c__resize_function__CiotpfManualTopic__publishers  // resize(index) function pointer
  }
};

static const rosidl_typesupport_introspection_c__MessageMembers CiotpfAppNodeInfo_Response__rosidl_typesupport_introspection_c__CiotpfAppNodeInfo_Response_message_members = {
  "ciotpf_messages__srv",  // message namespace
  "CiotpfAppNodeInfo_Response",  // message name
  2,  // number of fields
  sizeof(ciotpf_messages__srv__CiotpfAppNodeInfo_Response),
  CiotpfAppNodeInfo_Response__rosidl_typesupport_introspection_c__CiotpfAppNodeInfo_Response_message_member_array  // message members
};

// this is not const since it must be initialized on first access
// since C does not allow non-integral compile-time constants
static rosidl_message_type_support_t CiotpfAppNodeInfo_Response__rosidl_typesupport_introspection_c__CiotpfAppNodeInfo_Response_message_type_support_handle = {
  0,
  &CiotpfAppNodeInfo_Response__rosidl_typesupport_introspection_c__CiotpfAppNodeInfo_Response_message_members,
  get_message_typesupport_handle_function,
};

ROSIDL_TYPESUPPORT_INTROSPECTION_C_EXPORT_ciotpf_messages
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, ciotpf_messages, srv, CiotpfAppNodeInfo_Response)() {
  CiotpfAppNodeInfo_Response__rosidl_typesupport_introspection_c__CiotpfAppNodeInfo_Response_message_member_array[0].members_ =
    ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, ciotpf_messages, msg, CiotpfManualTopic)();
  CiotpfAppNodeInfo_Response__rosidl_typesupport_introspection_c__CiotpfAppNodeInfo_Response_message_member_array[1].members_ =
    ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, ciotpf_messages, msg, CiotpfManualTopic)();
  if (!CiotpfAppNodeInfo_Response__rosidl_typesupport_introspection_c__CiotpfAppNodeInfo_Response_message_type_support_handle.typesupport_identifier) {
    CiotpfAppNodeInfo_Response__rosidl_typesupport_introspection_c__CiotpfAppNodeInfo_Response_message_type_support_handle.typesupport_identifier =
      rosidl_typesupport_introspection_c__identifier;
  }
  return &CiotpfAppNodeInfo_Response__rosidl_typesupport_introspection_c__CiotpfAppNodeInfo_Response_message_type_support_handle;
}
#ifdef __cplusplus
}
#endif

#include "rosidl_generator_c/service_type_support_struct.h"
// already included above
// #include "ciotpf_messages/msg/rosidl_typesupport_introspection_c__visibility_control.h"
// already included above
// #include "ciotpf_messages/srv/ciotpf_app_node_info__rosidl_typesupport_introspection_c.h"
// already included above
// #include "rosidl_typesupport_introspection_c/identifier.h"
#include "rosidl_typesupport_introspection_c/service_introspection.h"

// this is intentionally not const to allow initialization later to prevent an initialization race
static rosidl_typesupport_introspection_c__ServiceMembers ciotpf_messages__srv__ciotpf_app_node_info__rosidl_typesupport_introspection_c__CiotpfAppNodeInfo_service_members = {
  "ciotpf_messages__srv",  // service namespace
  "CiotpfAppNodeInfo",  // service name
  // these two fields are initialized below on the first access
  NULL,  // request message
  // ciotpf_messages__srv__ciotpf_app_node_info__rosidl_typesupport_introspection_c__CiotpfAppNodeInfo_Request_message_type_support_handle,
  NULL  // response message
  // ciotpf_messages__srv__ciotpf_app_node_info__rosidl_typesupport_introspection_c__CiotpfAppNodeInfo_Response_message_type_support_handle
};

static rosidl_service_type_support_t ciotpf_messages__srv__ciotpf_app_node_info__rosidl_typesupport_introspection_c__CiotpfAppNodeInfo_service_type_support_handle = {
  0,
  &ciotpf_messages__srv__ciotpf_app_node_info__rosidl_typesupport_introspection_c__CiotpfAppNodeInfo_service_members,
  get_service_typesupport_handle_function,
};

// Forward declaration of request/response type support functions
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, ciotpf_messages, srv, CiotpfAppNodeInfo_Request)();

const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, ciotpf_messages, srv, CiotpfAppNodeInfo_Response)();

ROSIDL_TYPESUPPORT_INTROSPECTION_C_EXPORT_ciotpf_messages
const rosidl_service_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__SERVICE_SYMBOL_NAME(rosidl_typesupport_introspection_c, ciotpf_messages, srv, CiotpfAppNodeInfo)() {
  if (!ciotpf_messages__srv__ciotpf_app_node_info__rosidl_typesupport_introspection_c__CiotpfAppNodeInfo_service_type_support_handle.typesupport_identifier) {
    ciotpf_messages__srv__ciotpf_app_node_info__rosidl_typesupport_introspection_c__CiotpfAppNodeInfo_service_type_support_handle.typesupport_identifier =
      rosidl_typesupport_introspection_c__identifier;
  }
  rosidl_typesupport_introspection_c__ServiceMembers * service_members =
    (rosidl_typesupport_introspection_c__ServiceMembers *)ciotpf_messages__srv__ciotpf_app_node_info__rosidl_typesupport_introspection_c__CiotpfAppNodeInfo_service_type_support_handle.data;

  if (!service_members->request_members_) {
    service_members->request_members_ =
      (const rosidl_typesupport_introspection_c__MessageMembers *)
      ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, ciotpf_messages, srv, CiotpfAppNodeInfo_Request)()->data;
  }
  if (!service_members->response_members_) {
    service_members->response_members_ =
      (const rosidl_typesupport_introspection_c__MessageMembers *)
      ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, ciotpf_messages, srv, CiotpfAppNodeInfo_Response)()->data;
  }

  return &ciotpf_messages__srv__ciotpf_app_node_info__rosidl_typesupport_introspection_c__CiotpfAppNodeInfo_service_type_support_handle;
}
