// generated from rosidl_generator_cpp/resource/idl__traits.hpp.em
// with input from ciotpf_messages:srv/CiotpfAppNodeInfo.idl
// generated code does not contain a copyright notice

#ifndef CIOTPF_MESSAGES__SRV__CIOTPF_APP_NODE_INFO__TRAITS_HPP_
#define CIOTPF_MESSAGES__SRV__CIOTPF_APP_NODE_INFO__TRAITS_HPP_

#include "ciotpf_messages/srv/ciotpf_app_node_info__struct.hpp"
#include <rosidl_generator_cpp/traits.hpp>
#include <stdint.h>
#include <type_traits>

namespace rosidl_generator_traits
{

template<>
inline const char * data_type<ciotpf_messages::srv::CiotpfAppNodeInfo_Request>()
{
  return "ciotpf_messages::srv::CiotpfAppNodeInfo_Request";
}

template<>
struct has_fixed_size<ciotpf_messages::srv::CiotpfAppNodeInfo_Request>
  : std::integral_constant<bool, true> {};

template<>
struct has_bounded_size<ciotpf_messages::srv::CiotpfAppNodeInfo_Request>
  : std::integral_constant<bool, true> {};

}  // namespace rosidl_generator_traits

namespace rosidl_generator_traits
{

template<>
inline const char * data_type<ciotpf_messages::srv::CiotpfAppNodeInfo_Response>()
{
  return "ciotpf_messages::srv::CiotpfAppNodeInfo_Response";
}

template<>
struct has_fixed_size<ciotpf_messages::srv::CiotpfAppNodeInfo_Response>
  : std::integral_constant<bool, false> {};

template<>
struct has_bounded_size<ciotpf_messages::srv::CiotpfAppNodeInfo_Response>
  : std::integral_constant<bool, false> {};

}  // namespace rosidl_generator_traits

namespace rosidl_generator_traits
{

template<>
inline const char * data_type<ciotpf_messages::srv::CiotpfAppNodeInfo>()
{
  return "ciotpf_messages::srv::CiotpfAppNodeInfo";
}

template<>
struct has_fixed_size<ciotpf_messages::srv::CiotpfAppNodeInfo>
  : std::integral_constant<
    bool,
    has_fixed_size<ciotpf_messages::srv::CiotpfAppNodeInfo_Request>::value &&
    has_fixed_size<ciotpf_messages::srv::CiotpfAppNodeInfo_Response>::value
  >
{
};

template<>
struct has_bounded_size<ciotpf_messages::srv::CiotpfAppNodeInfo>
  : std::integral_constant<
    bool,
    has_bounded_size<ciotpf_messages::srv::CiotpfAppNodeInfo_Request>::value &&
    has_bounded_size<ciotpf_messages::srv::CiotpfAppNodeInfo_Response>::value
  >
{
};

}  // namespace rosidl_generator_traits

#endif  // CIOTPF_MESSAGES__SRV__CIOTPF_APP_NODE_INFO__TRAITS_HPP_
