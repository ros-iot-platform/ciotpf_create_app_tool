// generated from rosidl_generator_cpp/resource/idl__struct.hpp.em
// with input from ciotpf_messages:srv/CiotpfAppNodeInfo.idl
// generated code does not contain a copyright notice

#ifndef CIOTPF_MESSAGES__SRV__CIOTPF_APP_NODE_INFO__STRUCT_HPP_
#define CIOTPF_MESSAGES__SRV__CIOTPF_APP_NODE_INFO__STRUCT_HPP_

#include <rosidl_generator_cpp/bounded_vector.hpp>
#include <rosidl_generator_cpp/message_initialization.hpp>
#include <algorithm>
#include <array>
#include <memory>
#include <string>
#include <vector>

// Protect against ERROR being predefined on Windows, in case somebody defines a
// constant by that name.
#if defined(_WIN32)
  #if defined(ERROR)
    #undef ERROR
  #endif
  #if defined(NO_ERROR)
    #undef NO_ERROR
  #endif
#endif

#ifndef _WIN32
# define DEPRECATED__ciotpf_messages__srv__CiotpfAppNodeInfo_Request __attribute__((deprecated))
#else
# define DEPRECATED__ciotpf_messages__srv__CiotpfAppNodeInfo_Request __declspec(deprecated)
#endif

namespace ciotpf_messages
{

namespace srv
{

// message struct
template<class ContainerAllocator>
struct CiotpfAppNodeInfo_Request_
{
  using Type = CiotpfAppNodeInfo_Request_<ContainerAllocator>;

  explicit CiotpfAppNodeInfo_Request_(rosidl_generator_cpp::MessageInitialization _init = rosidl_generator_cpp::MessageInitialization::ALL)
  {
    if (rosidl_generator_cpp::MessageInitialization::ALL == _init ||
      rosidl_generator_cpp::MessageInitialization::ZERO == _init)
    {
      this->structure_needs_at_least_one_member = 0;
    }
  }

  explicit CiotpfAppNodeInfo_Request_(const ContainerAllocator & _alloc, rosidl_generator_cpp::MessageInitialization _init = rosidl_generator_cpp::MessageInitialization::ALL)
  {
    (void)_alloc;
    if (rosidl_generator_cpp::MessageInitialization::ALL == _init ||
      rosidl_generator_cpp::MessageInitialization::ZERO == _init)
    {
      this->structure_needs_at_least_one_member = 0;
    }
  }

  // field types and members
  using _structure_needs_at_least_one_member_type =
    uint8_t;
  _structure_needs_at_least_one_member_type structure_needs_at_least_one_member;

  // setters for named parameter idiom
  Type & set__structure_needs_at_least_one_member(
    const uint8_t & _arg)
  {
    this->structure_needs_at_least_one_member = _arg;
    return *this;
  }

  // constant declarations

  // pointer types
  using RawPtr =
    ciotpf_messages::srv::CiotpfAppNodeInfo_Request_<ContainerAllocator> *;
  using ConstRawPtr =
    const ciotpf_messages::srv::CiotpfAppNodeInfo_Request_<ContainerAllocator> *;
  using SharedPtr =
    std::shared_ptr<ciotpf_messages::srv::CiotpfAppNodeInfo_Request_<ContainerAllocator>>;
  using ConstSharedPtr =
    std::shared_ptr<ciotpf_messages::srv::CiotpfAppNodeInfo_Request_<ContainerAllocator> const>;

  template<typename Deleter = std::default_delete<
      ciotpf_messages::srv::CiotpfAppNodeInfo_Request_<ContainerAllocator>>>
  using UniquePtrWithDeleter =
    std::unique_ptr<ciotpf_messages::srv::CiotpfAppNodeInfo_Request_<ContainerAllocator>, Deleter>;

  using UniquePtr = UniquePtrWithDeleter<>;

  template<typename Deleter = std::default_delete<
      ciotpf_messages::srv::CiotpfAppNodeInfo_Request_<ContainerAllocator>>>
  using ConstUniquePtrWithDeleter =
    std::unique_ptr<ciotpf_messages::srv::CiotpfAppNodeInfo_Request_<ContainerAllocator> const, Deleter>;
  using ConstUniquePtr = ConstUniquePtrWithDeleter<>;

  using WeakPtr =
    std::weak_ptr<ciotpf_messages::srv::CiotpfAppNodeInfo_Request_<ContainerAllocator>>;
  using ConstWeakPtr =
    std::weak_ptr<ciotpf_messages::srv::CiotpfAppNodeInfo_Request_<ContainerAllocator> const>;

  // pointer types similar to ROS 1, use SharedPtr / ConstSharedPtr instead
  // NOTE: Can't use 'using' here because GNU C++ can't parse attributes properly
  typedef DEPRECATED__ciotpf_messages__srv__CiotpfAppNodeInfo_Request
    std::shared_ptr<ciotpf_messages::srv::CiotpfAppNodeInfo_Request_<ContainerAllocator>>
    Ptr;
  typedef DEPRECATED__ciotpf_messages__srv__CiotpfAppNodeInfo_Request
    std::shared_ptr<ciotpf_messages::srv::CiotpfAppNodeInfo_Request_<ContainerAllocator> const>
    ConstPtr;

  // comparison operators
  bool operator==(const CiotpfAppNodeInfo_Request_ & other) const
  {
    if (this->structure_needs_at_least_one_member != other.structure_needs_at_least_one_member) {
      return false;
    }
    return true;
  }
  bool operator!=(const CiotpfAppNodeInfo_Request_ & other) const
  {
    return !this->operator==(other);
  }
};  // struct CiotpfAppNodeInfo_Request_

// alias to use template instance with default allocator
using CiotpfAppNodeInfo_Request =
  ciotpf_messages::srv::CiotpfAppNodeInfo_Request_<std::allocator<void>>;

// constant definitions

}  // namespace srv

}  // namespace ciotpf_messages

// Protect against ERROR being predefined on Windows, in case somebody defines a
// constant by that name.
#if defined(_WIN32)
  #if defined(ERROR)
    #undef ERROR
  #endif
  #if defined(NO_ERROR)
    #undef NO_ERROR
  #endif
#endif

// Include directives for member types
// Member 'subscriptions'
// Member 'publishers'
#include "ciotpf_messages/msg/ciotpf_manual_topic__struct.hpp"

#ifndef _WIN32
# define DEPRECATED__ciotpf_messages__srv__CiotpfAppNodeInfo_Response __attribute__((deprecated))
#else
# define DEPRECATED__ciotpf_messages__srv__CiotpfAppNodeInfo_Response __declspec(deprecated)
#endif

namespace ciotpf_messages
{

namespace srv
{

// message struct
template<class ContainerAllocator>
struct CiotpfAppNodeInfo_Response_
{
  using Type = CiotpfAppNodeInfo_Response_<ContainerAllocator>;

  explicit CiotpfAppNodeInfo_Response_(rosidl_generator_cpp::MessageInitialization _init = rosidl_generator_cpp::MessageInitialization::ALL)
  {
    (void)_init;
  }

  explicit CiotpfAppNodeInfo_Response_(const ContainerAllocator & _alloc, rosidl_generator_cpp::MessageInitialization _init = rosidl_generator_cpp::MessageInitialization::ALL)
  {
    (void)_init;
    (void)_alloc;
  }

  // field types and members
  using _subscriptions_type =
    std::vector<ciotpf_messages::msg::CiotpfManualTopic_<ContainerAllocator>, typename ContainerAllocator::template rebind<ciotpf_messages::msg::CiotpfManualTopic_<ContainerAllocator>>::other>;
  _subscriptions_type subscriptions;
  using _publishers_type =
    std::vector<ciotpf_messages::msg::CiotpfManualTopic_<ContainerAllocator>, typename ContainerAllocator::template rebind<ciotpf_messages::msg::CiotpfManualTopic_<ContainerAllocator>>::other>;
  _publishers_type publishers;

  // setters for named parameter idiom
  Type & set__subscriptions(
    const std::vector<ciotpf_messages::msg::CiotpfManualTopic_<ContainerAllocator>, typename ContainerAllocator::template rebind<ciotpf_messages::msg::CiotpfManualTopic_<ContainerAllocator>>::other> & _arg)
  {
    this->subscriptions = _arg;
    return *this;
  }
  Type & set__publishers(
    const std::vector<ciotpf_messages::msg::CiotpfManualTopic_<ContainerAllocator>, typename ContainerAllocator::template rebind<ciotpf_messages::msg::CiotpfManualTopic_<ContainerAllocator>>::other> & _arg)
  {
    this->publishers = _arg;
    return *this;
  }

  // constant declarations

  // pointer types
  using RawPtr =
    ciotpf_messages::srv::CiotpfAppNodeInfo_Response_<ContainerAllocator> *;
  using ConstRawPtr =
    const ciotpf_messages::srv::CiotpfAppNodeInfo_Response_<ContainerAllocator> *;
  using SharedPtr =
    std::shared_ptr<ciotpf_messages::srv::CiotpfAppNodeInfo_Response_<ContainerAllocator>>;
  using ConstSharedPtr =
    std::shared_ptr<ciotpf_messages::srv::CiotpfAppNodeInfo_Response_<ContainerAllocator> const>;

  template<typename Deleter = std::default_delete<
      ciotpf_messages::srv::CiotpfAppNodeInfo_Response_<ContainerAllocator>>>
  using UniquePtrWithDeleter =
    std::unique_ptr<ciotpf_messages::srv::CiotpfAppNodeInfo_Response_<ContainerAllocator>, Deleter>;

  using UniquePtr = UniquePtrWithDeleter<>;

  template<typename Deleter = std::default_delete<
      ciotpf_messages::srv::CiotpfAppNodeInfo_Response_<ContainerAllocator>>>
  using ConstUniquePtrWithDeleter =
    std::unique_ptr<ciotpf_messages::srv::CiotpfAppNodeInfo_Response_<ContainerAllocator> const, Deleter>;
  using ConstUniquePtr = ConstUniquePtrWithDeleter<>;

  using WeakPtr =
    std::weak_ptr<ciotpf_messages::srv::CiotpfAppNodeInfo_Response_<ContainerAllocator>>;
  using ConstWeakPtr =
    std::weak_ptr<ciotpf_messages::srv::CiotpfAppNodeInfo_Response_<ContainerAllocator> const>;

  // pointer types similar to ROS 1, use SharedPtr / ConstSharedPtr instead
  // NOTE: Can't use 'using' here because GNU C++ can't parse attributes properly
  typedef DEPRECATED__ciotpf_messages__srv__CiotpfAppNodeInfo_Response
    std::shared_ptr<ciotpf_messages::srv::CiotpfAppNodeInfo_Response_<ContainerAllocator>>
    Ptr;
  typedef DEPRECATED__ciotpf_messages__srv__CiotpfAppNodeInfo_Response
    std::shared_ptr<ciotpf_messages::srv::CiotpfAppNodeInfo_Response_<ContainerAllocator> const>
    ConstPtr;

  // comparison operators
  bool operator==(const CiotpfAppNodeInfo_Response_ & other) const
  {
    if (this->subscriptions != other.subscriptions) {
      return false;
    }
    if (this->publishers != other.publishers) {
      return false;
    }
    return true;
  }
  bool operator!=(const CiotpfAppNodeInfo_Response_ & other) const
  {
    return !this->operator==(other);
  }
};  // struct CiotpfAppNodeInfo_Response_

// alias to use template instance with default allocator
using CiotpfAppNodeInfo_Response =
  ciotpf_messages::srv::CiotpfAppNodeInfo_Response_<std::allocator<void>>;

// constant definitions

}  // namespace srv

}  // namespace ciotpf_messages

namespace ciotpf_messages
{

namespace srv
{

struct CiotpfAppNodeInfo
{
  using Request = ciotpf_messages::srv::CiotpfAppNodeInfo_Request;
  using Response = ciotpf_messages::srv::CiotpfAppNodeInfo_Response;
};

}  // namespace srv

}  // namespace ciotpf_messages

#endif  // CIOTPF_MESSAGES__SRV__CIOTPF_APP_NODE_INFO__STRUCT_HPP_
