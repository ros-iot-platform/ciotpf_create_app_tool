
from re import template
from typing import Dict, List
import stringcase

class CiotpfTopicsConfig:
    topics_necessary_attibutes = {"Name": str, "TopicName": str, "Type": str, "MultipleTopic": bool, "Comment": str}
    allowed_types = ["Bool", "Float", "Int", "String", "Any"]

    #ここにAnyは悪手な気がする・・・
    allowed_types_in_ros = {"Bool": "Bool", "Float": "Float32", "Int":"Int32", "String": "String", "Any": "Any"}
    
    def check_attributes(topic_dict: Dict, is_input: bool) -> None:

        for attribute in CiotpfTopicsConfig.topics_necessary_attibutes:

            #NameなりTopicNameなりが存在するか
            if attribute not in topic_dict:
                raise Exception(str(topic_dict) + "\n"+attribute+" is not in config file")
                
            #型があっているか。Nameならstrだし、Numberならint
            if type(topic_dict[attribute]) is not CiotpfTopicsConfig.topics_necessary_attibutes[attribute]:
                raise Exception(str(topic_dict) + "\n"+attribute+" wrong type")

        if topic_dict["Type"] not in CiotpfTopicsConfig.allowed_types:
            raise Exception(topic_dict["Name"] + ": " + topic_dict["Type"] +"Typeにだめなやつが指定されてるよ！")

        #TopicNameにワイルドカード含まれてるならMultipleTopicはTrueじゃないとだめ
        if "*" in topic_dict["TopicName"]:
            if topic_dict["MultipleTopic"] == False:
                raise Exception(topic_dict["Name"]+ ": MultipleTopic should be True. TopicNameにワイルドカード含まれてるならTrueじゃないとだめ！")
        
        #wild card入ってない＆manualでないなら、MultipleTopicは必ずFalse
        if ("*" not in topic_dict["TopicName"]) and (topic_dict["TopicName"] != "manual"):
            if topic_dict["MultipleTopic"] == True:
                raise Exception(topic_dict["Name"]+ ": MultipleTopic should be False. wild card入ってない＆manualでないなら、MultipleTopicは必ずFalse")
        
        if topic_dict["TopicName"] != "manual":
            if topic_dict["TopicName"][0] != "~" and topic_dict["TopicName"][0] != "/":
                raise Exception(topic_dict["Name"] + "  topic name should start from ~ or /")

        if not is_input:
            if topic_dict["Type"] == "Any":
                raise Exception(topic_dict["Name"]+ ": outputなのにAnyはだめ！")
            
    def __init__(self, topic_dict: Dict, is_input: bool) -> None:
        CiotpfTopicsConfig.check_attributes(topic_dict, is_input) 
        self.is_input = is_input

        self.name:str = topic_dict["Name"]
        self.topicName:str = topic_dict["TopicName"]
        self.topicType:str = topic_dict["Type"]
        self.multiple_topic:bool = topic_dict["MultipleTopic"]
        self.comment:str = topic_dict["Comment"]

        self.is_manual = self.topicName == "manual"
        self.is_wildcard = "*" in self.topicName

    def code_generation_create_datamodels(self, appName: str) -> str:
        name_snakecase = stringcase.snakecase(self.name)
        app_name_snakecase = stringcase.snakecase(appName)
        _type_str = self.allowed_types_in_ros[self.topicType]

        #Subscriberのとき
        if self.is_input:
            _callback_function_name = name_snakecase + "_subscription_callback"


            if self.is_manual:
                _topicname = "/ciotpf/config/app/%s/manual_topics/subscribe/%s" %(app_name_snakecase, name_snakecase)
                _is_multiple = "True" if self.multiple_topic else "False"

                _template ='ManualSubscriptionDataModel("%s", "%s", %s, self.%s, %s)' % (
                    self.name,
                    _topicname,
                    _is_multiple,
                    _callback_function_name,
                    _type_str)
                return _template

            elif self.is_wildcard:
                if self.topicName[0] == "~":
                    raise NotImplementedError()

                _topicname = "/ciotpf" + self.topicName
                _template = 'WildCardSubscriptionDataModel("%s", "%s", self.%s, %s)' %(
                    self.name,
                    _topicname,
                    _callback_function_name,
                    _type_str)
                return _template
            else:
                _topicname = ""
                if self.topicName[0] == "~":
                    _topicname = self.topicName.replace("~","/ciotpf/app/"+app_name_snakecase)
                else:
                    _topicname = "/ciotpf" + self.topicName
                    
                _template = 'StaticSubscriptionDataModel("%s", self.%s, %s)' % (
                    _topicname, 
                    _callback_function_name, 
                    _type_str)
                return _template

        #Publisherのとき
        else:
            if self.is_manual:
                _topicname = "/ciotpf/config/app/%s/manual_topics/publish/%s" %(app_name_snakecase, name_snakecase)
                _is_multiple = "True" if self.multiple_topic else "False"

                _template = 'ManualPublisherDataModel("%s", "%s", %s, %s)' % (
                    self.name,
                    _topicname,
                    _is_multiple,
                    _type_str)
                return _template

            elif self.is_wildcard:
                raise Exception("NOO")
            else:
                _topicname = ""
                if self.topicName[0] == "~":
                    _topicname = self.topicName.replace("~","/ciotpf/app/"+app_name_snakecase)
                else:
                    _topicname = "/ciotpf" + self.topicName

                
                _template = 'StaticPublisherDataModel("AllStatus","/ciotpf/app/light_controll_app/all_status", String)'
                _template = 'StaticPublisherDataModel("%s","%s", %s)' %(
                    self.name,
                    _topicname,
                    _type_str)
                return _template



        raise Exception("NOOO")
        return ""

    #callback関数とか、publisher関数とかの生成
    def code_generation_functions(self) -> str:
        name_snakecase = stringcase.snakecase(self.name)
        _type_str = self.allowed_types_in_ros[self.topicType]   

        #Subscriptionのとき。callback関数つくるぞ
        if self.is_input:
            _callback_function_name = name_snakecase + "_subscription_callback"

            _template = '#%s \n' \
                        '    def %s(self, topicName: str, message: %s) -> None: \n' \
                        '        raise NotImplementedError() \n' %(
                self.comment,
                _callback_function_name,
                _type_str
            )
            return _template

        #Publisherのとき        
        else:
            _publisher_function_name = name_snakecase + "_publish"
            if self.is_manual:
                _template = '#%s \n' \
                            '    def %s(self, value: %s) -> None:\n'\
                            '        pub: List[ManualPublisherDataModel] = [pub for pub in self.manual_publushers if pub.name == "%s"]\n'\
                            '        if len(pub) != 1: \n'\
                            '            raise Exception("WHAAAA") \n'\
                            '        self._publish(pub[0], value)\n ' %(
                                    self.comment,
                                    _publisher_function_name,
                                    _type_str,
                                    self.name
                                )

                return _template
            else:
                _template = '#%s \n' \
                            '    def %s(self, value: %s) -> None:\n'\
                            '        pub: List[StaticPublisherDataModel] = [pub for pub in self.static_publishers if pub.name == "%s"]\n'\
                            '        if len(pub) != 1: \n'\
                            '            raise Exception("WHAAAA") \n'\
                            '        self._publish(pub[0], value)\n ' %(
                                    self.comment,
                                    _publisher_function_name,
                                    _type_str,
                                    self.name
                                )
                return _template

        raise Exception("??????????//")
    
    #publisherのときのみ、publishする関数の定義をコメントとして残すのでそれを生成
    def code_generation_comments(self) -> str:
        if self.is_input:
            raise Exception("NOOOOOO")

        name_snakecase = stringcase.snakecase(self.name)
        _type_str = self.allowed_types_in_ros[self.topicType]   
        _publisher_function_name = name_snakecase + "_publish"

        _template = '#%s \n' \
                    '    #def %s(self, value: %s) -> None:\n' % (
                        self.comment,
                        _publisher_function_name,
                        _type_str,
                    )
        return _template


class CiotpfAppNodeConfig:
    neccesarry_attributes = ["AppName", "Inputs", "Outputs", "NodeDescription"]

    def check_attributes(config_dict: Dict) -> None:
        for attribute in CiotpfAppNodeConfig.neccesarry_attributes:
            if attribute not in config_dict:
                raise Exception( attribute+" is not in config file")
        
            
    def __init__(self, config_dict: Dict) -> None:
        CiotpfAppNodeConfig.check_attributes(config_dict)

        self.input_list:List[CiotpfTopicsConfig] = [CiotpfTopicsConfig(topic, True) for topic in config_dict["Inputs"]]
        self.output_list:List[CiotpfTopicsConfig] = [CiotpfTopicsConfig(topic, False) for topic in config_dict["Outputs"]]
        self.appName: str = config_dict["AppName"]
        self.nodeDescription: str = config_dict["NodeDescription"]


        
        