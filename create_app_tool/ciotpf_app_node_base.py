
#App Nodeのベースクラス
import os
import sys
import time
import rclpy
from rclpy.node import Node
from rclpy.publisher import Publisher
from rclpy.subscription import Subscription
from diagnostic_msgs.msg import KeyValue
from std_msgs.msg import Bool, String, Int32, Float32
from ciotpf_ros2_utils.ciotpf_ros2_utils import AvailableTopic, get_topics_by_topic_name, get_topics_by_topic_name_and_type
from ciotpf_messages.srv import CiotpfAppNodeInfo, CiotpfAppNodeManualTopicConfig
from ciotpf_messages.srv._ciotpf_app_node_info import CiotpfAppNodeInfo_Request
from ciotpf_messages.srv._ciotpf_app_node_info import CiotpfAppNodeInfo_Response
from ciotpf_messages.srv._ciotpf_app_node_manual_topic_config import CiotpfAppNodeManualTopicConfig_Request
from ciotpf_messages.srv._ciotpf_app_node_manual_topic_config import CiotpfAppNodeManualTopicConfig_Response

from ciotpf_messages.msg import CiotpfManualTopic, CiotpfStringArray

import functools

from typing import List, Callable,TypeVar, Optional, Union, cast, Dict, Any

TopicType = Union[Bool, Int32, Float32, String, Any]
TopicTypePython = Union[bool, int, float, str, Any]
TopicTypeNames: Dict[TopicType, str] = {Bool: "std_msgs/msg/Bool", Int32: "std_msgs/msg/Int32", Float32: "std_msgs/msg/Float32", String: "std_msgs/msg/String"}


class StaticSubscriptionDataModel:
    def __init__(self, topic_name: str, callback:Callable[[str, TopicType], None], topic_type: TopicType):

        self.topic_name = topic_name
        self.callback = callback
        self.topic_type: TopicType = topic_type

        self.subscription: Optional[Subscription] = None 

class ManualSubscriptionDataModel:

    #configのTopicName例:　/ciotpf/config/app/light_controll_app/manual_topics/lights_status
    #is_multiple: 複数のトピックをサブスクライブするかどうか。
    def __init__(self, name: str, config_topic_name: str, is_multiple: bool,callback:Callable[[str, TopicType], None], topic_type: TopicType):
        self.name = name
        self.config_topic_name = config_topic_name
        self.is_multiple = is_multiple
        self.callback = callback
        self.topic_type = topic_type

        self.subscriptions: List[Subscription] = []

class WildCardSubscriptionDataModel:

    def __init__(self, name: str, topic_name: str, callback:Callable[[str, TopicType], None], topic_type: TopicType):
        self.name = name
        self.topic_name = topic_name
        self.callback =callback
        self.topic_type = topic_type

        self.subscriptions: List[Subscription] = []

class StaticPublisherDataModel:
    def __init__(self, name: str, topic_name: str, topic_type: TopicType):
        self.name: str = name
        self.topic_name: str = topic_name
        self.topic_type: TopicType = topic_type

        self.publisher: Optional[Publisher] = None 

class ManualPublisherDataModel:

    #is_multiple: 複数のトピックをpublishするかどうか
    def __init__(self, name: str, config_topic_name: str, is_multiple: bool, topic_type: TopicType):
        self.name =name
        self.config_topic_name = config_topic_name
        self.is_multiple = is_multiple
        self.topic_type = topic_type

        self.publishers: List[Publisher] = []


PublisherType = Union[ManualPublisherDataModel, StaticPublisherDataModel]
SubscriptionType = Union[ManualSubscriptionDataModel, StaticSubscriptionDataModel]


class CiotpfAppNodeBase(Node):
    TIMER_PERIOD = 0.5 # 定期処理の間隔

    def __init__(self, node_name, app_name):
        super().__init__(node_name)

        self.static_subscriptions: List[StaticSubscriptionDataModel] = []
        self.manual_subscriptions: List[ManualSubscriptionDataModel]= []
        self.wildcard_subscriptions: List[WildCardSubscriptionDataModel] = []
        self.static_publishers: List[StaticPublisherDataModel] = []
        self.manual_publushers: List[ManualPublisherDataModel] = []
        
        self.appNodeInfoService = self.create_service(CiotpfAppNodeInfo, '/ciotpf/app/%s/app_node_info' %(app_name), self._app_node_info_service_callback)
        self.manualTopicConfigService = self.create_service(CiotpfAppNodeManualTopicConfig, '/ciotpf/app/%s/manual_topic_config' %(app_name), self._manual_topic_config_service_callback)

        self._timer = self.create_timer(CiotpfAppNodeBase.TIMER_PERIOD, self._periodic_task)


    def _periodic_task(self) -> None:
        self.create_wildcard_subscriptions(self.wildcard_subscriptions)
                

    def create_static_subscriptions(self, static_subscriptions: List[StaticSubscriptionDataModel]):
        for subs in static_subscriptions:
            s = self.create_subscription(subs.topic_type, subs.topic_name, 
                                    functools.partial(subs.callback,subs.topic_name),10)
            subs.subscription = s

    #これは直接subscriptionを作るのではなく、subscriptionを作るためのsubscriptionを作っている。
    #ここで指定したconfig_topic_nameに値が飛んでくると、その値に入ってるtopic nameがサブスクライブできたりサブスクライブ外したりできる
    #def create_manual_subscriptions(self, manual_subscriptions: List[ManualSubscriptionDataModel]):
    #    for subs in manual_subscriptions:
    #        self.create_subscription(KeyValue, subs.config_topic_name, 
    #         functools.partial(self._manual_subscription_topics_settings_callback, subs), 10)

    #マッチするトピックを探してサブスクライブしていく。
    #これは定期的に呼ぼうね。(初回起動時と、後は定期実行)
    def create_wildcard_subscriptions(self, wildcard_subscriptions: List[WildCardSubscriptionDataModel]):

        for wildcard_subscription in wildcard_subscriptions:
            _wildcard_topic_name = wildcard_subscription.topic_name

            if wildcard_subscription.topic_type == Any:
                matched_topics: List[AvailableTopic] = get_topics_by_topic_name(self, _wildcard_topic_name)
            else:
                _topic_type_str = TopicTypeNames[wildcard_subscription.topic_type]
                matched_topics: List[AvailableTopic] = get_topics_by_topic_name_and_type(self, _wildcard_topic_name, _topic_type_str)

            for matched_topic in matched_topics:
                subs = [s for s in wildcard_subscription.subscriptions if s.topic == matched_topic.topicName]
                #subscribe済みなら何もしない
                if len(subs) == 1:
                    continue
                elif len(subs) > 1:
                    raise Exception("NOOO")

                # 見つけたトピックの型が使えるやつかどうか確認
                _t = None
                for k, v in TopicTypeNames.items():
                    if v == matched_topic.topicType:
                        _t = k
                        break

                if _t is None:
                    continue
                
                subs = self.create_subscription(_t,
                    matched_topic.topicName,
                    functools.partial(wildcard_subscription.callback,matched_topic.topicName),10)
                wildcard_subscription.subscriptions.append(subs)
        


    def create_static_publishers(self, static_publishers: List[StaticPublisherDataModel]):
        for pubs in static_publishers:
            p = self.create_publisher(pubs.topic_type, pubs.topic_name, 10)
            pubs.publisher = p


    def _app_node_info_service_callback(self, request: CiotpfAppNodeInfo_Request, response: CiotpfAppNodeInfo_Response) -> CiotpfAppNodeInfo_Response:
        
        manual_topic_subscriptions: List[CiotpfManualTopic] = []
        manual_topic_publishers: List[CiotpfManualTopic] = []

        for manual_subscription in self.manual_subscriptions:
            topic_names: List[str] = []
            for sub in manual_subscription.subscriptions:
                topic_names.append(sub.topic)

            manualTopic = CiotpfManualTopic()
            manualTopic.name = manual_subscription.name
            manualTopic.topic_type = TopicTypeNames[manual_subscription.topic_type]

            tmp = CiotpfStringArray()
            tmp.data = topic_names
            manualTopic.topic_names = tmp

            manual_topic_subscriptions.append(manualTopic)

        for manual_publisher in self.manual_publushers:
            topic_names: List[str] = []
            for sub in manual_publisher.publishers:
                topic_names.append(sub.topic)

            manualTopic = CiotpfManualTopic()
            manualTopic.name = manual_publisher.name
            manualTopic.topic_type = TopicTypeNames[manual_publisher.topic_type]

            tmp = CiotpfStringArray()
            tmp.data = topic_names
            manualTopic.topic_names = tmp
            
            manual_topic_publishers.append(manualTopic)
        
        response.publishers = manual_topic_publishers
        response.subscriptions = manual_topic_subscriptions
        return response
            
    def _manual_topic_config_service_callback(self, request: CiotpfAppNodeManualTopicConfig_Request, response: CiotpfAppNodeManualTopicConfig_Response) -> CiotpfAppNodeManualTopicConfig_Response:
        print("setting manual topic service called")
        isSubscription: bool = request.is_subscription
        isAdd: bool = request.is_add
        topicName: str = request.topic_name
        name: str = request.name

        #subscriptionの登録,削除
        if isSubscription:
            _manual_subscriptions: List[ManualSubscriptionDataModel] = [s for s in self.manual_subscriptions if s.name == name]
            if len(_manual_subscriptions) == 0:
                print("invalid!!!")
                response.is_success = False
                response.message = "name not found"
                return response
            elif len(_manual_subscriptions) > 1:
                response.is_success = False
                response.message = "internal error"
                print("multiple manual subscription with same name")
                #ほんとはException投げて死にたいけど、response返さないとだめだから。。。どうしよ
                # TODO
                return response
                #raise Exception("multiple manual subscription with same name")
            
            manual_subscription = _manual_subscriptions[0]
            subs =[s for s in manual_subscription.subscriptions if s.topic == topicName]

            #登録            
            if isAdd:
                #すでにsubscribeされてた
                if len(subs) != 0:
                    response.is_success = False 
                    response.message = "already subscribed"
                    return response
                subscription = self.create_subscription(manual_subscription.topic_type, topicName,
                                                    functools.partial(manual_subscription.callback,topicName), 10)
                manual_subscription.subscriptions.append(subscription)
                response.is_success = True 
                response.message = "success"
                return response
            #削除
            else:
                if len(subs) != 1:
                    response.is_success = False 
                    response.message = "topic name not subscribed"
                    return response
                subs[0].destroy()
                manual_subscription.subscriptions.remove(subs[0])
                response.is_success = True 
                response.message = "success"
                return response

        #publisherの登録,削除
        else:
            _manual_publishers: List[ManualPublisherDataModel] = [p for p in self.manual_publushers if p.name == name]
            if len(_manual_publishers) == 0:
                print("invalid!!!")
                response.is_success = False
                response.message = "name not found"
                return response
            elif len(_manual_publishers) > 1:
                response.is_success = False
                response.message = "internal error"
                print("multiple manual publisher with same name")
                #ほんとはException投げて死にたいけど、response返さないとだめだから。。。どうしよ
                # TODO
                return response
            manual_publisher = _manual_publishers[0]
            pubs = [p for p in manual_publisher.publishers if p.topic == topicName]

            if isAdd:
                #すでに登録されていた
                if len(pubs) != 0:
                    response.is_success = False 
                    response.message = "already registerd"
                    return response
                
                publisher = self.create_publisher(manual_publisher.topic_type, topicName, 10)
                manual_publisher.publishers.append(publisher)
                response.is_success = True 
                response.message = "success"
                return response
            else:
                if len(pubs) != 1:
                    response.is_success = False 
                    response.message = "topic name not registerd"
                    return response
                pubs[0].destroy()
                manual_publisher.publishers.remove(pubs[0])
                response.is_success = True 
                response.message = "success"
                return response
        

        response.is_success = False
        response.message = "internal error"
        return response

    #TopicType = Union[Bool, Int32, Float32, String]
    def _publish(self, publisher: PublisherType, data: TopicType):

        #data: TopicType = None
        #if publisher.topic_type == Bool:
        #    if type(publish_value) != bool:
        #        raise Exception("Type incompatible")
        #    
        #    data = Bool()
        #    data.data = publish_value
        #
        #elif publisher.topic_type == Int32:
        #    if type(publish_value) != int:
        #        raise Exception("Type incompatible")
        #
        #    data = Int32()
        #    data.data = publish_value
        #    
        #elif publisher.topic_type == Float32:
        #    if type(publish_value) != float:
        #        raise Exception("Type incompatible")
        #
        #    data = Float32()
        #    data.data = publish_value
        #    
        #elif publisher.topic_type == String:
        #    if type(publish_value) != str:
        #        raise Exception("Type incompatible")
        #
        #    data = String()
        #    data.data = publish_value
            

        if type(publisher) == StaticPublisherDataModel:
            static_pub: StaticPublisherDataModel = cast(StaticPublisherDataModel, publisher)
            if static_pub.publisher is not None:
                static_pub.publisher.publish(data)
            return
        
        if type(publisher == ManualPublisherDataModel):
            manual_pub: ManualPublisherDataModel = cast(ManualPublisherDataModel, publisher)

            if manual_pub.is_multiple:
                for p in manual_pub.publishers:
                    p.publish(data)
            else:
                if len(manual_pub.publishers) > 1:
                    raise Exception("eeeee")

                if len(manual_pub.publishers) == 1:
                    manual_pub.publishers[0].publish(data)

        




