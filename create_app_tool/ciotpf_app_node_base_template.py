
#spec_example.cpfc.yamlをゴニョゴニョした結果の出力がこうなっててほしい！

import rclpy
from rclpy.node import Node
from rclpy.publisher import Publisher
from rclpy.subscription import Subscription
from std_msgs.msg import Bool, String, Float32, Int32
from diagnostic_msgs.msg import KeyValue
import functools
from typing import List, Callable,TypeVar, Optional, Any
from create_app_tool.ciotpf_app_node_base import CiotpfAppNodeBase, StaticSubscriptionDataModel, ManualSubscriptionDataModel, StaticPublisherDataModel, ManualPublisherDataModel, WildCardSubscriptionDataModel

class _--APP_NAME_PASCALCASE--AppNode(CiotpfAppNodeBase):

    APP_NAME = "--APP_NAME_SNAKECASE--"
    NODE_NAME = "--APP_NAME_SNAKECASE--_app_node"

    
    def __init__(self) -> None:

        super().__init__(self.NODE_NAME, self.APP_NAME)


        #Subscriptions
        self.static_subscriptions: List[StaticSubscriptionDataModel] = [
            --STATIC_SUBSCRIPTION_DATAMODELS--
        ]
        self.manual_subscriptions= [
            --MANUAL_SUBSCRIPTION_DATAMODELS--
        ]
        
        self.wildcard_subscriptions: List[WildCardSubscriptionDataModel] = [
            --WILDCARD_SUBSCRIPTION_DATAMODELS--
        ]

        #Publishers
        self.static_publishers: List[StaticPublisherDataModel] = [
            --STATIC_PUBLISHER_DATAMODELS--
        ]
        self.manual_publushers: List[ManualPublisherDataModel] = [
            --MANUAL_PUBLISHER_DATAMODELS--
        ]

        self.create_static_subscriptions(self.static_subscriptions)
        self.create_wildcard_subscriptions(self.wildcard_subscriptions)
        self.create_static_publishers(self.static_publishers)


    # TIMER_PERIODの間隔で勝手に呼ばれる
    def _periodic_task(self) -> None:
        super()._periodic_task()

        --STATIC_SUBSCRIPTION_CALLBACKS--
    
        --MANUAL_SUBSCRIPTION_CALLBACKS--

        --WILDCARD_SUBSCRIPTION_CALLBACKS--

        --STATIC_PUBLISHER_FUNCTIONS--

        --MANUAL_PUBLISHER_FUNCTIONS--

