import yaml
import sys
import pathlib
import os
import argparse 
import functools
import shutil
from ciotpf_ros2_utils.ciotpf_ros2_utils import get_all_topics, get_topics_by_topic_name
from ciotpf_app_node_config import CiotpfAppNodeConfig

from typing import Dict, TypeVar
from yaml import YAMLObject

import stringcase

script_dir = os.path.abspath(os.path.dirname(__file__))


#baseになる方のファイルを生成。 _{appname}AppNode クラス
def create_base_file(template: str, app_node_config:CiotpfAppNodeConfig) -> str:

    app_name_pascal_case = stringcase.pascalcase(app_node_config.appName)
    app_name_snake_case = stringcase.snakecase(app_node_config.appName)

    static_subscription_datamodels_str = ""
    manual_subscription_datamodels_str = ""
    wildcard_subscription_datamodels_str = ""

    static_publisher_datamodels_str = ""
    manual_publisher_datamodels_str = ""

    static_subscription_callback_str = ""
    manual_subscription_callback_str = ""
    wildcard_subscription_callback_str = ""

    static_publisher_function_str = ""
    manual_publisher_function_str = ""


    static_subscription_topics = [t for t in app_node_config.input_list if t.is_wildcard == False and t.is_manual == False]
    manual_subscription_topics = [t for t in app_node_config.input_list if t.is_manual == True]
    wildcard_subscription_topics = [t for t in app_node_config.input_list if t.is_wildcard == True]

    static_publisher_topics = [t for t in app_node_config.output_list if t.is_wildcard == False and t.is_manual == False]
    manual_publisher_topics = [t for t in app_node_config.output_list if t.is_manual == True]


    for topicConfig in static_subscription_topics:
        _tmp = topicConfig.code_generation_create_datamodels(app_node_config.appName)
        static_subscription_datamodels_str += "\n            " + _tmp
    
    for topicConfig in manual_subscription_topics:
        _tmp = topicConfig.code_generation_create_datamodels(app_node_config.appName)
        manual_subscription_datamodels_str += "\n            " + _tmp
        
    for topicConfig in wildcard_subscription_topics:
        _tmp = topicConfig.code_generation_create_datamodels(app_node_config.appName)
        wildcard_subscription_datamodels_str += "\n            " + _tmp


    for topicConfig in static_publisher_topics:
        _tmp = topicConfig.code_generation_create_datamodels(app_node_config.appName)
        static_publisher_datamodels_str += "\n            " + _tmp

    for topicConfig in manual_publisher_topics:
        _tmp = topicConfig.code_generation_create_datamodels(app_node_config.appName)
        manual_publisher_datamodels_str += "\n            " + _tmp

    
    #####functions
    for topicConfig in static_subscription_topics:
        _tmp = topicConfig.code_generation_functions()
        static_subscription_callback_str += "\n   " + _tmp
    
    for topicConfig in manual_subscription_topics:
        _tmp = topicConfig.code_generation_functions()
        manual_subscription_callback_str += "\n   " + _tmp
        
    for topicConfig in wildcard_subscription_topics:
        _tmp = topicConfig.code_generation_functions()
        wildcard_subscription_callback_str += "\n   " + _tmp


    for topicConfig in static_publisher_topics:
        _tmp = topicConfig.code_generation_functions()
        static_publisher_function_str += "\n    " + _tmp

    for topicConfig in manual_publisher_topics:
        _tmp = topicConfig.code_generation_functions()
        manual_publisher_function_str += "\n    " + _tmp

    

    template = template.replace("--APP_NAME_PASCALCASE--", app_name_pascal_case)
    template = template.replace("--APP_NAME_SNAKECASE--", app_name_snake_case)

    template = template.replace("--STATIC_SUBSCRIPTION_DATAMODELS--", static_subscription_datamodels_str)
    template = template.replace("--MANUAL_SUBSCRIPTION_DATAMODELS--", manual_subscription_datamodels_str)
    template = template.replace("--WILDCARD_SUBSCRIPTION_DATAMODELS--", wildcard_subscription_datamodels_str)
    template = template.replace("--STATIC_PUBLISHER_DATAMODELS--", static_publisher_datamodels_str)
    template = template.replace("--MANUAL_PUBLISHER_DATAMODELS--", manual_publisher_datamodels_str)

    template = template.replace("--STATIC_SUBSCRIPTION_CALLBACKS--", static_subscription_callback_str)
    template = template.replace("--MANUAL_SUBSCRIPTION_CALLBACKS--", manual_subscription_callback_str)
    template = template.replace("--WILDCARD_SUBSCRIPTION_CALLBACKS--", wildcard_subscription_callback_str)
    template = template.replace("--STATIC_PUBLISHER_FUNCTIONS--", static_publisher_function_str)
    template = template.replace("--MANUAL_PUBLISHER_FUNCTIONS--", manual_publisher_function_str)


    return template

#子になる方のファイルを生成。 {appname}AppNode クラス
def create_file(template: str, app_node_config:CiotpfAppNodeConfig) -> str:
    app_name_pascal_case = stringcase.pascalcase(app_node_config.appName)

    static_publisher_function_comments_str = ""
    manual_publisher_function_comments_str = ""

    static_subscription_callback_str = ""
    manual_subscription_callback_str = ""
    wildcard_subscription_callback_str = ""

    static_subscription_topics = [t for t in app_node_config.input_list if t.is_wildcard == False and t.is_manual == False]
    manual_subscription_topics = [t for t in app_node_config.input_list if t.is_manual == True]
    wildcard_subscription_topics = [t for t in app_node_config.input_list if t.is_wildcard == True]

    static_publisher_topics = [t for t in app_node_config.output_list if t.is_wildcard == False and t.is_manual == False]
    manual_publisher_topics = [t for t in app_node_config.output_list if t.is_manual == True]


    ####publish function comments
    for topicConfig in static_publisher_topics:
        _tmp = topicConfig.code_generation_comments()
        static_publisher_function_comments_str += "\n    " + _tmp
        
    for topicConfig in manual_publisher_topics:
        _tmp = topicConfig.code_generation_comments()
        manual_publisher_function_comments_str += "\n    " + _tmp

    #####callbacks
    for topicConfig in static_subscription_topics:
        _tmp = topicConfig.code_generation_functions()
        static_subscription_callback_str += "\n   " + _tmp
    
    for topicConfig in manual_subscription_topics:
        _tmp = topicConfig.code_generation_functions()
        manual_subscription_callback_str += "\n   " + _tmp
        
    for topicConfig in wildcard_subscription_topics:
        _tmp = topicConfig.code_generation_functions()
        wildcard_subscription_callback_str += "\n   " + _tmp

    #####
    template = template.replace("--APP_NAME_PASCALCASE--", app_name_pascal_case)

    template = template.replace("--STATIC_PUBLISHER_FUNCTION_COMMENTS--", static_publisher_function_comments_str)
    template = template.replace("--MANUAL_PUBLISHER_FUNCTION_COMMENTS--", manual_publisher_function_comments_str)


    
    template = template.replace("--STATIC_SUBSCRIPTION_CALLBACKS--", static_subscription_callback_str)
    template = template.replace("--MANUAL_SUBSCRIPTION_CALLBACKS--", manual_subscription_callback_str)
    template = template.replace("--WILDCARD_SUBSCRIPTION_CALLBACKS--", wildcard_subscription_callback_str)    

    return template

def main(config_file_path: str, output_file_path: str):
    config_dict:Dict = {} 
    base_template: str = ""
    template: str = ""

    try:
        with open(config_file_path) as file:
            config_dict = yaml.safe_load(file)
            
    except Exception as e:
        print(e)
        print('Exception occurred while loading YAML...', file=sys.stderr)
        return

    try:
        _base_template_path = os.path.join(script_dir, "ciotpf_app_node_base_template.py")
        with open(_base_template_path) as file:
            base_template = file.read()
            
    except Exception as e:
        print(e)
        print('Exception occurred while base template file', file=sys.stderr)
        return

    try:
        _app_node_template_path = os.path.join(script_dir, "ciotpf_app_node_template.py")
        with open(_app_node_template_path) as file:
            template = file.read()
            
    except Exception as e:
        print(e)
        print('Exception occurred while template file', file=sys.stderr)
        return
    ######

    app_node_config:CiotpfAppNodeConfig = CiotpfAppNodeConfig(config_dict)
    base_result = create_base_file(base_template, app_node_config)
    result = create_file(template, app_node_config)
    

    ######
    app_name_pascal_case = stringcase.pascalcase(app_node_config.appName)


    output_dir_path = os.path.join(output_file_path, app_name_pascal_case)
    basefile_path = os.path.join(output_dir_path, "libs","_"+app_name_pascal_case+".py")
    file_path = os.path.join(output_dir_path, app_name_pascal_case + ".py")
    dockerfile_path = os.path.join(output_dir_path,"libs", "Dockerfile")
    pip_requirementstxt_path = os.path.join(output_dir_path, "libs","requirements.txt")

    try:
        os.makedirs(output_dir_path,exist_ok=False)
        os.makedirs(output_dir_path + "/libs",exist_ok=False)

        with open(basefile_path, mode="x") as f:
            f.write(base_result)
        with open(file_path, mode="x") as f:
            f.write(result)

        _dockerfile_template_path = os.path.join(script_dir, "dockerfile_template")
        shutil.copyfile(_dockerfile_template_path, dockerfile_path)
        _requirementstxt_template_path = os.path.join(script_dir, "requirements.txt_template")
        shutil.copyfile(_requirementstxt_template_path, pip_requirementstxt_path)
        
    except Exception as e:
        print(e)
        print("Error on creating files", file=sys.stderr)
        return

#実行例. pathは相対パスでも絶対パスでもOK.　相対パスのときは、実行した位置からの相対パス。
#python3 create_app_tool/main.py ./spec_example.cpfc.yaml ./test_output/
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='このプログラムの説明（なくてもよい）')  
    parser.add_argument('input_file', help='仕様書ファイル')    # 必須の引数を追加
    parser.add_argument('output_file_dir', help='出力ファイルのディレクトリ')

    args = parser.parse_args()

    input_file = args.input_file
    output_file_dir = args.output_file_dir 

    input_file_path = pathlib.Path(input_file)
    output_file_dir_path = pathlib.Path(output_file_dir)

    if input_file_path.is_dir():
        raise Exception("ファイルを指定して！")
    
    if output_file_dir_path.is_file():
        raise Exception("出力はディレクトリを指定して！")

    if not input_file_path.is_absolute():
        input_file_path = input_file_path.resolve()
    if not output_file_dir_path.is_absolute():
        output_file_dir_path = output_file_dir_path.resolve()


    input_file_path = str(input_file_path)
    output_file_dir_path = str(output_file_dir_path)

    main(input_file_path, output_file_dir_path)
    #main("../spec_example2.cpfc.yaml", "../test_output/")