import rclpy
from std_msgs.msg import Bool, String, Float32
from diagnostic_msgs.msg import KeyValue
from libs._--APP_NAME_PASCALCASE-- import _--APP_NAME_PASCALCASE--AppNode
from typing import Any

class --APP_NAME_PASCALCASE--AppNode(_--APP_NAME_PASCALCASE--AppNode):

    --STATIC_PUBLISHER_FUNCTION_COMMENTS--
    --MANUAL_PUBLISHER_FUNCTION_COMMENTS--


    def __init__(self):
        #親のinitを最初に呼ぶ
        super().__init__()
        ##

    --STATIC_SUBSCRIPTION_CALLBACKS--
    
    --MANUAL_SUBSCRIPTION_CALLBACKS--

    --WILDCARD_SUBSCRIPTION_CALLBACKS--
    



def main(args=None):
    rclpy.init(args=args)

    node = --APP_NAME_PASCALCASE--AppNode()

    rclpy.spin(node)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()