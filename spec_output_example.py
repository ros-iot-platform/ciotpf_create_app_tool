
#spec_example.cpfc.yamlをゴニョゴニョした結果の出力がこうなっててほしい！

import rclpy
from rclpy.node import Node
from rclpy.publisher import Publisher
from rclpy.subscription import Subscription
from std_msgs.msg import Bool, String
from diagnostic_msgs.msg import KeyValue
import functools
from typing import List, Callable,TypeVar, Optional
from create_app_tool.ciotpf_app_node_base import CiotpfAppNodeBase, StaticSubscriptionDataModel, ManualSubscriptionDataModel, StaticPublisherDataModel, ManualPublisherDataModel, WildCardSubscriptionDataModel

class _LightControlAppNode(CiotpfAppNodeBase):
    NODE_NAME = "light_control_app_node"

    
    def __init__(self) -> None:

        super().__init__(self.NODE_NAME)


        #Subscriptions
        self.static_subscriptions: List[StaticSubscriptionDataModel] = [
            StaticSubscriptionDataModel("/ciotpf/app/light_controll_app/switch_all", self.switch_controll_subscription_callback, Bool),
        ]
        self.manual_subscriptions= [
            ManualSubscriptionDataModel("LightsStatus", 
                "/ciotpf/config/app/light_controll_app/manual_topics/lights_status",
                True,
                self.lights_status_subscription_callback, Bool)
        ]
        
        self.wildcard_subscriptions: List[WildCardSubscriptionDataModel] = [
        ]

        #Publishers
        self.static_publishers: List[StaticPublisherDataModel] = [
            StaticPublisherDataModel("AllStatus","/ciotpf/app/light_controll_app/all_status", String)
        ]
        self.manual_publushers: List[ManualPublisherDataModel] = [
            ManualPublisherDataModel("Lights", "/ciotpf/config/app/light_controll_app/manual_topics/lights", True, Bool)
        ]



        self.create_static_subscriptions(self.static_subscriptions)
        self.create_manual_subscriptions(self.manual_subscriptions)
        self.create_wildcard_subscriptions(self.wildcard_subscriptions)
        self.create_static_publishers(self.static_publishers)
        self.create_manual_publishers(self.manual_publushers)


    # TIMER_PERIODの間隔で勝手に呼ばれる
    def _periodic_task(self) -> None:
        pass

    #コントロールしてる照明を全て消すor付ける
    def switch_controll_subscription_callback(self, topicName: str, message: Bool) -> None:
        raise NotImplementedError()


    #コントロールするライトの現在のステータスたち(OnなのかOffなのか)
    def lights_status_subscription_callback(self, topicName: str, message: Bool) -> None:
        raise NotImplementedError()

    #コントロール対象の照明が受け付けている、照明のオンオフ操作トピック
    def lights_publish(self, value: bool) -> None:
        pub: List[ManualPublisherDataModel] = [pub for pub in self.manual_publushers if pub.name == "Lights"]
        if len(pub) != 1:
            raise Exception("WHAAAA")
        self._publish(pub[0], value)
       

    #全ての照明のステータス
    def all_status_publish(self, value: str) -> None:
        pub: List[StaticPublisherDataModel] = [pub for pub in self.static_publishers if pub.name == "AllStatus"]
        if len(pub) != 1:
            raise Exception("WAHATTT")
        self._publish(pub[0], value)
    


class LightControlAppNode(_LightControlAppNode):
    #コントロール対象の照明が受け付けている、照明のオンオフ操作トピック
    #lights_publish(self, value: bool) -> None

    #全ての照明のステータス
    #all_status_publish(self, value: str) -> String


    def __init__(self):
        super().__init__()


    #コントロールしてる照明を全て消すor付ける
    def switch_controll_subscription_callback(self, topicName: str, message: Bool) -> None:
        if message.data:
            print("ぜんぶ付けるよ！")
        else:
            print("ぜんぶけす！")
        self.lights_publish(message.data)
        #raise NotImplementedError()


    #コントロールするライトの現在のステータスたち(OnなのかOffなのか)
    def lights_status_subscription_callback(self, topicName: str, message: Bool) -> None:
        print(topicName, "  ", message.data)
        
        #raise NotImplementedError()




def main(args=None):
    rclpy.init(args=args)

    node = LightControlAppNode()

    rclpy.spin(node)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()


