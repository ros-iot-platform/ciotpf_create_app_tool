
#spec_example.cpfc.yamlをゴニョゴニョした結果の出力がこうなっててほしい！

import rclpy
from rclpy.node import Node
from rclpy.publisher import Publisher
from rclpy.subscription import Subscription

from std_msgs.msg import Bool, String, Float32
from diagnostic_msgs.msg import KeyValue
import functools
from typing import List
from create_app_tool.ciotpf_app_node_base import CiotpfAppNodeBase, StaticSubscriptionDataModel, ManualSubscriptionDataModel, StaticPublisherDataModel, ManualPublisherDataModel, WildCardSubscriptionDataModel

class _TemperatureMonitorAppNode(CiotpfAppNodeBase):
    NODE_NAME = "temperature_monitor_app_node"

    def __init__(self) -> None:

        super().__init__(self.NODE_NAME)
        

        #Subscriptions
        self.static_subscriptions: List[StaticSubscriptionDataModel] = [
        ]

        self.manual_subscriptions: List[ManualSubscriptionDataModel] = [
        ]

        self.wildcard_subscriptions: List[WildCardSubscriptionDataModel] = [
            WildCardSubscriptionDataModel("Thermometers", "/ciotpf/sensor/*/temperature", self.termometers_subscription_callback, Float32)
        ]

        #Publishers
        self.static_publishers: List[StaticPublisherDataModel] = [
            StaticPublisherDataModel("AverageTemperature", "/ciotpf/app/light_controll_app/temerature/average", Float32)
        ]

        self.manual_publushers: List[ManualPublisherDataModel] = [
        ]

        self.create_static_subscriptions(self.static_subscriptions)
        self.create_manual_subscriptions(self.manual_subscriptions)
        self.create_wildcard_subscriptions(self.wildcard_subscriptions)
        self.create_static_publishers(self.static_publishers)
        self.create_manual_publishers(self.manual_publushers)


    
    def _periodic_task(self) -> None:
        super()._periodic_task()
        

    def termometers_subscription_callback(self, topicName:str, message: Float32):
        raise NotImplementedError()

    #１時間の平均気温
    def average_temperature_publish(self, value: float) -> String:
        pub: List[StaticPublisherDataModel] = [pub for pub in self.static_publishers if pub.name == "AverageTemperature"]
        if len(pub) != 1:
            raise Exception("WAHATTT")
        self._publish(pub[0], value)




class TemperatureMonitorAppNode(_TemperatureMonitorAppNode):
    #コントロール対象の照明が受け付けている、照明のオンオフ操作トピック
    #lights_publish(self, value: bool) -> None

    #全ての照明のステータス
    #all_status_publish(self, value: str) -> String


    def __init__(self):
        super().__init__()

    #温度計の温度情報
    def termometers_subscription_callback(self, topicName:str, message: Float32):
        print(message.data)




def main(args=None):
    rclpy.init(args=args)

    node = TemperatureMonitorAppNode()

    rclpy.spin(node)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()